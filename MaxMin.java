/**Gabriela Mizrahi-Arnaud, Ben Gellman and Erez Krimsky.
 * gmizrah1, bgellma1, ekrimsk1
 * Data Structures Section 2
 * Project 4
 *
 * Class keeps track of max and min for color components of set of pixels.
 */

public class MaxMin {

    /**Int array of MaxRGB stored as [maxRed,maxGreen,maxBlue].*/
    private int[] maxRGB;

    /**Int array of MinRGB stored as [minRed,minGreen,minBlue].*/
    private int[] minRGB;
    /** Number of components in segment.*/ 
    private int size;

   /**Constructor if only one component.
    * @param rGB RGB of this component.
    */  
    public MaxMin(int[] rGB) {
        this.maxRGB = new int[rGB.length];
        for (int i = 0; i < rGB.length; i++) {
            this.maxRGB[i] = rGB[i];
        }
       
        this.minRGB = new int[rGB.length];
        for (int i = 0; i < rGB.length; i++) {
            this.minRGB[i] = rGB[i];
        }
        this.size = 1;
    }
    /**Constructor if merging to components.
     * @param max maxRGB of components
     * @param min minRGB of components
     * @param numElem number of elements in both components
     */
    public MaxMin(int[] max, int[] min, int numElem) {
        this.maxRGB = new int[max.length];
        for (int i = 0; i < max.length; i++) {
            this.maxRGB[i] = max[i];
        }
       
        this.minRGB = new int[min.length];
        for (int i = 0; i < min.length; i++) {
            this.minRGB[i] = min[i];
        }
        this.size = numElem;
            
    }
    /**Returns size.
     * @return int
     */ 
    public int getSize() {
        return this.size;
    }
   /**Returns diff between max and min.
    * @return int[]
    */ 
    public int[] diff() {
        int[] diff = new int[this.maxRGB.length];
        for (int i = 0; i < diff.length; i++) {
            diff[i] = this.maxRGB[i] - this.minRGB[i];
        }
        return diff;
    }
    /** Returns maxRGB.
     * @return int[]
     */
    public int[] getMaxRGB() {
        return this.maxRGB;
    }
    /** Returns minRGB.
     * @return int[]
     */
    public int[] getMinRGB() {
        return this.minRGB;
    } 
   /**toString.
    * @return String
    */   
    public String toString() {
        String toString = "";
        toString = toString + "Max: ( ";
        for (int i = 0; i < this.maxRGB.length; i++) {
            toString = toString + this.maxRGB[i] + ",";
        }
        toString = toString + ")\n";
        toString = toString + "Min: ( ";
        for (int i = 0; i < this.maxRGB.length; i++) {
            toString = toString + this.minRGB[i] + ",";
        }
        toString = toString + ")\n";
        toString = toString + "Size: " + this.size + "\n";
        return toString;
    }
}
