import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * 
 * @author 
 *
 * @param <VT>
 */
public class WGraphP4<VT> implements WGraph<VT> {

    
    /** Used to sequentially generate vertex IDs for this graph! */
    private int nextID;

    /** A list of all the vertices. */
  //  private ArrayList<GVertex<VT>> verts;
    private HashSet<GVertex<VT>> verts;

    /** The current graphID.*/
    private int vertexID;
    
    /** A list of all the edges. */
    private ArrayList<WEdge<VT>> edges;
    
    /** Hashmap storing adjacency lists. */
    private HashMap<Integer, AdjListEntry> adjList;
    
    /** The number of edges. */   
    private int numEdges;
    
    /** Sets the capacity for maximum number of vertices. */
    private int maxVerts; 
    
    /** Weather or not the graph is directed. */
    private boolean directed;

    /** 
     * Private internal class for entries of adjacency list.
     */
    private class AdjListEntry { //fix checkstyle on this line 

        /** The origin (first entry) of the adjacency list. */
        private GVertex<VT> origin;

        /** The ID of the origin of the adjacency list. */
        private int originId;

        /** List of adjacent vertices for given vertex . */
        private ArrayList<GVertex<VT>> adjacents;

        /**
         * AdjListEntry constructor. 
         * @param orig The origin for the adjacency list. 
         */
        AdjListEntry(GVertex<VT> orig) {
            this.origin = orig;
            this.originId = orig.id();
            this.adjacents = new ArrayList<GVertex<VT>>();
        }

        /**
         * Add a vertex to the graph. 
         * @param v the vertex to be added 
         * @return true if added, false otherwise. 
         */
        public boolean addVert(GVertex<VT> v) {
            if (!this.adjacents.contains(v)) {
                this.adjacents.add(v);
                return true;
            }
            return false;
        }

        /**
         * Remove a specific vertex from the graph.
         * @param v the vertex to be removed from the graph.
         * @return true if removed, false otherwise.
         */
        public boolean removeVert(GVertex<VT> v) {
            if (!this.adjacents.contains(v)) {
                return false;
            }
            this.adjacents.remove(v);
            return true;
        }

        /**
         * Gives the vertices adjacent to a specific vertex.
         * @return a list of adjecent vertices.
         */
        public ArrayList<GVertex<VT>> getAdjacents() {
            return this.adjacents;
        }

        /**
         * Gives origin ID. 
         * @return int
         */
        public int getOrigId() {
            return this.originId;
        }
        
    }

    // ----- End of Internal Class----------//
    /**
     * WGraphP4 constructor. 
     */
    public WGraphP4() {
        this.nextID = 0;
        this.numEdges = 0;
        //this.verts = new ArrayList<GVertex<VT>>(maximumVerts);
        this.verts = new HashSet<GVertex<VT>>();
        this.adjList = new HashMap<Integer, AdjListEntry>();
        this.edges = new ArrayList<WEdge<VT>>();
        //this.maxVerts = 1000;
        this.directed = false;
        this.vertexID = 0;
    }
    /**
     * WGraphP4 constructor. 
     * @param maximumVerts the maximum number of vertices. 
     */
    public WGraphP4(int maximumVerts) {
        this.nextID = 0;
        this.numEdges = 0;
        //this.verts = new ArrayList<GVertex<VT>>(maximumVerts);
        this.verts = new HashSet<GVertex<VT>>();
        this.adjList = new HashMap<Integer, AdjListEntry>();
        this.edges = new ArrayList<WEdge<VT>>();
        this.maxVerts = maximumVerts;
        this.directed = false;
        this.vertexID = 0;
    }

    
    /** Get the number of vertices. 
     *  @return the number
     */
    @Override
    public int numVerts() {
        return this.verts.size();
    }

    
    /** Get the number of edges. 
     *  @return the number
     */
    @Override
    public int numEdges() {
        return this.numEdges;
    }

    /** Get the next ID to use in making a vertex. 
     *  @return the id
     */
    @Override
    public int nextID() {
        return this.nextID++;
        //return ++nextID; // not sure if makes dif.
    }
   
    /** Create and add a vertex to the graph.
     *  @param d the data to store in the vertex
     *  @return true if successful, false otherwise
     */
    @Override
    public boolean addVertex(Object data) {
        GVertex<VT> temp = new GVertex(data, this.nextID++); // check this 
        this.addVertex(temp);
        
        return true;
    }

    
    @Override
    public boolean addVertex(GVertex<VT> v) {
        //if (this.verts.contains(v)) {
        if (this.verts.contains(v)) {
            return false; // there
        }
        v.setGraphID(this.vertexID);
        this.vertexID++;
        // add to list of vertices 
        //System.out.println("vertex added");
        
        //this.verts.add(v);
        this.verts.add(v);
        
        // put in adjacency list 
        this.adjList.put((Integer) v.id(), new AdjListEntry(v));
        //System.out.println("Added vertex: " + v);
        return true;
    }

    @Override
    public boolean addEdge(GVertex<VT> v, GVertex<VT> u, double weight) {
       //should this use generics?
        WEdge<VT> temp = new WEdge<VT>(v, u, weight); 
        boolean success = this.addEdge(temp);
        return success;
    }

    @Override
    public boolean addEdge(WEdge<VT> e) {
        boolean added = false;
        boolean added2 = false;
        added = this.addEdge(e.source(), e.end());
        added2 = this.addEdge(e.end(), e.source());
        //this.adjList.get(e.source().id()).addVert(e.end());
        if (added) {
            this.edges.add(e);
        }
        if (added2 && e.isDirected()) {
            this.deleteEdge(e.end(), e.source());
            this.numEdges--;

            //this.adjList.get(e.end().id()).addVert(e.source());
            this.numEdges--; // don't count it twice
        } else if (added2) {
            this.edges.add(new WEdge(e.end(), e.source(), e.weight()));
            this.numEdges--;
        }
        return added;
    }

    // could this end up adding only one edge if one is already there?
    /**
     * Add edge given two vertices. 
     * @param v first vertex
     * @param u second vertex
     * @return boolean
     */
    public boolean addEdge(GVertex<VT> v, GVertex<VT> u) { 
        boolean success = true;
        //if (!this.verts.contains(v)) {
        if (!this.verts.contains(v)) {
            success = this.addVertex(v);

        }
        
        //if (success && !this.verts.contains(u)) {
        if (success && !this.verts.contains(u)) {
            success = this.addVertex(u);

            //System.out.println("BARK");
        }
        if (!success) {
            return false;
        }
        // put the edge in, if not already there
        // if (! this.matrix[v.id()][u.id()]) {

        
        //System.out.println("V is :" + this.adjList.get(v.id()));
        //System.out.println("U is :" + this.adjList.get(u.id()));
        AdjListEntry vList = this.adjList.get(v.id());
        AdjListEntry uList = this.adjList.get(u.id());
        //System.out.println("V: " +  vList.getAdjacents().toString());
        //System.out.println("U: " +  uList.getAdjacents().toString());
        
        // this try catch might not really work as it should!!!!
        try {
            if (!(vList.getAdjacents().contains(u))) {
                    //&& !(uList.getAdjacents().contains(v))) {
                this.adjList.get(v.id()).addVert(u);
                //this.adjList.get(u.id()).addVert(v);
                this.numEdges++;
                return true;
            }
        } catch (NullPointerException e) {
            System.out.println("NULL POINTER EXCEPTION");
            return true;
        }

        return false; // was already there
    }

    
    /** Remove an edge if there.  
     *  @param v the starting vertex
     *  @param u the ending vertex
     *  @return true if delete, false if wasn't there
     */
    @Override
    public boolean deleteEdge(GVertex<VT> v, GVertex<VT> u) {
        //if (this.verts.contains(v) && this.verts.contains(u)) {
        if (this.verts.contains(v) && this.verts.contains(u)) {
            // if (this.matrix[v.id()][u.id()]) {

            if (this.adjList.get(v.id()).getAdjacents().contains(u)) {
                this.adjList.get(v.id()).removeVert(u);
                this.numEdges--;
                for (int i = 0; i < this.edges.size(); i++) {
                    if ((this.edges.get(i).source() == v 
                            && this.edges.get(i).end() == u)) {
                        this.edges.remove(i);
                    }
                }
          
                if (!this.directed) {
                    this.deleteDirected(v, u);
                }
            }
            return true;

        }
        return false;
    }

    /** Helper method for deleteEdge to reduce cyclomatic complexity.
     * @param v the starting vertex
     * @param u the ending vertex
     */

    public void deleteDirected(GVertex<VT> v, GVertex<VT> u) {
        if (this.adjList.get(u.id()).getAdjacents().contains(v)) {
            this.adjList.get(u.id()).removeVert(v);
            for (int i = 0; i < this.edges.size(); i++) {
                if ((this.edges.get(i).end() == v 
                            && this.edges.get(i).source() == u
                            && !this.directed)) {
                    this.edges.remove(i);
                }
            }
        }
    }

    
    /** Return true if there is an edge between v and u. 
     *  @param v the starting vertex
     *  @param u the ending vertex
     *  @return true if there is an edge between them, false otherwise
     */
    @Override
    public boolean areAdjacent(GVertex<VT> v, GVertex<VT> u) {
        // return this.matrix[v.id()][u.id()];
        // would this give the wrong result if the graph is directed?
        return (this.adjList.get(v.id()).getAdjacents().contains(u)
                && this.adjList.get(u.id()).getAdjacents().contains(v));
    }

    

    /** Return a list of all the neighbors of vertex v.  
     *  @param v the vertex source
     *  @return the neighboring vertices
     */
    @Override
    public ArrayList<GVertex<VT>> neighbors(GVertex<VT> v) {
       // ArrayList<GVertex> nbs = new ArrayList<GVertex>(this.numVerts());
        /*
         * int row = v.id(); for (int col=0; col < matrix.length; col++) { if
         * (this.matrix[row][col]) { // add vertex associated with col to nbs
         * nbs.add(this.verts.get(col)); } }
         */
        // return nbs;
        return this.adjList.get(v.id()).getAdjacents();
    }

    /** Return the number of edges incident to v.  
     *  @param v the vertex source
     *  @return the number of incident edges
     */
    @Override
    public int degree(GVertex<VT> v) {
        return this.neighbors(v).size();
    }

    /** See if an edge and vertex are incident to each other.
     *  @param e the edge
     *  @param v the vertex to check
     *  @return true if v is an endpoint of edge e
     */
    @Override
    public boolean areIncident(WEdge<VT> e, GVertex<VT> v) {
        return e.source().id() == v.id() || e.end().id() == v.id();
    }

    /** Return a list of all the edges.  
     *  @return the list
     */
    @Override
    public List<WEdge<VT>> allEdges() {
        /*
         * int nv = this.numVerts(); ArrayList<WEdge> edges = new
         * ArrayList<WEdge>(nv); /* for (int r = 0; r < nv; r++) { for (int c =
         * 0; c < nv; c++) { if (this.matrix[r][c]) { // there is an edge, add
         * to list edges.add(new WEdge(this.verts.get(r), this.verts.get(c),1));
         * } // will create duplicate edges for an undirected graph } }
         */
        // HashSet<WEdge> edges = new HashSet<WEdge>(nv);

        // will create duplicate edges for an undirected graph
        //
        /*
         * GVertex temp; ArrayList<GVertex> templist; for (int i = 0; i <
         * this.verts.size(); i++) { temp = this.verts.get(i); templist =
         * this.adjList.get(temp.id()).getAdjacents(); for (int j = 0; j <
         * templist.size(); j++) { edges.add(new WEdge(this.verts.get(i),
         * templist.get(j), 1)); } }
         */
        return this.edges;
    }

    /** Return a list of all the vertices.  
     *  @return the list
     */
    @Override
    public List<GVertex<VT>> allVertices() {
        ArrayList<GVertex<VT>> allVertices = new ArrayList<>();
        allVertices.addAll(this.verts);
        return allVertices;
    }

    /** Return a list of all the vertices that can be reached from v,
     *  in the order in which they would be visited in a depth-first
     *  search starting at v.  
     *  @param v the starting vertex
     *  @return the list of reachable vertices
     */
    @Override
    public List<GVertex<VT>> depthFirst(GVertex<VT> v) {
        ArrayList<GVertex<VT>> reaches = new 
                      ArrayList<GVertex<VT>>(this.numVerts());
        // using LinkedList<Vertex> as a Stack
        LinkedList<GVertex<VT>> stack = new LinkedList<GVertex<VT>>();
        boolean[] visited = new boolean[this.numVerts()]; // inits to false
        stack.addFirst(v);
        visited[v.getGraphID()] = true;
        while (!stack.isEmpty()) {
            v = stack.removeFirst();
            visited[v.getGraphID()] = true;
            reaches.add(v);
            for (GVertex<VT> u : this.neighbors(v)) {
                if (!visited[u.getGraphID()]) {
                    //visited[u.getGraphID()] = true;
                    stack.addFirst(u);
                    //System.out.println("added: " + u.data());
                }
            }
        }
        return reaches;
    }

    
    /** Return a list of all the edges incident on vertex v.  
     *  @param v the starting vertex
     *  @return the incident edges
     */
    @Override
    public List<WEdge<VT>> incidentEdges(GVertex<VT> v) {
        List<WEdge<VT>> temp = new ArrayList<WEdge<VT>>();
        for (int i = 0; i < this.edges.size(); i++) {
            if (this.edges.get(i).source().id() == v.id()) {
                temp.add(this.edges.get(i));
            }
        }
        return temp;
    }

    //--------------------------------- Kruskals --------------------------- // 
    
    
    /** Return a list of edges in a minimum spanning forest by
     *  implementing Kruskal's algorithm using fast union/finds.
     *  @return a list of the edges in the minimum spanning forest
     */
    @Override 
    public List<WEdge<VT>> kruskals() {
        // create an array of size this.maxVerts for the union/find operations 
   
        PQHeap<WEdge<VT>> edgePQ = new PQHeap<WEdge<VT>>();
        edgePQ.init(this.edges); // 
        
        List<WEdge<VT>> mstEdgeList = new ArrayList<>(); 
        if (this.numVerts() == 0) {
            return mstEdgeList;
        }
        
       // Partition myPart = new Partition();
        Partition myPart = new Partition(this.numVerts());
       
        WEdge<VT> temp;
        GVertex<VT> v;
        GVertex<VT> u;
        while (!edgePQ.isEmpty()) {
            temp = edgePQ.remove();
            v = temp.source();
            u = temp.end();
      //System.out.println("v id: "+v.id());
      //System.out.println("u id: "+u.id());
            if (myPart.find(v.getGraphID()) != myPart.find(u.getGraphID())) {
              //  System.out.println("Find v at " + v.id() + " :" 
              //      + myPart.find(v.id()));
               // System.out.println("Find u at " + u.id() + " :" 
               //     + myPart.find(u.id()));
        //        System.out.println("added " + temp.toString());
                
                mstEdgeList.add(temp);
                myPart.union(u.getGraphID(), v.getGraphID());
          //      System.out.println(myPart.toString());
                //System.out.println(Arrays.toString(array));
            }
        }
        return mstEdgeList;
    }
    
  
    
    // might be way easier to define a partition internal class

} // end class def 
