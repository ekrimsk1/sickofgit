/** Gabriela Mizrahi-Arnaud, Erez Krimsky, Ben Gellman.
 *  gmizrah1, ekrimsk1, bgelma1
 *  Data Structures Section 2
 *  Project 4
 *  This class implements the PriorityQueue interface as a heap.
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

/**This class implements the PriorityQueue interface as a heap.
 * @param <T> generic
 */
public class PQHeap<T extends Comparable<? super T>> 
                               implements PriorityQueue<T> {

    /**ArrayList to hold heap structure.*/
    private ArrayList<T> heap;
    
    /** Number of elements added to the heap.
     *  Since index 0 will be null, this size will be one less than heap.size()
     *  size is also the last index in the ArrayList
     */
    private int size;

    /** Comparator object used to compare items. */
    private Comparator<T> comp;

    /** Constructor, uses default comparator.*/
    public PQHeap() {
        this.heap = new ArrayList<T>();
        this.size = 0;
        this.comp = new Comparator<T>() {
            public int compare(T t1, T t2) {
                return t1.compareTo(t2);
            }
        }; //ok
    }
    
    /** Constructor that takes a comparator as an argument.
     *  @param c comparator
     */ 
    public PQHeap(final Comparator<T> c) { // final added to make eclpise happy 
        this.heap = new ArrayList<T>();
        this.size = 0;
        this.comp = new Comparator<T>() {
            public int compare(T t1, T t2) {
                return c.compare(t1, t2);
            }
        };

    }
    
    
    
    /** Insert a value. Duplicate values <b>do</b> end up in the
     *  queue, so inserting X three times means it has to be removed
     *  three times before it's gone again.
     *  @param t Value to add.
     */
    public void insert(T t) {
        this.size++;
        //want to add first element into index 1, leave 0 empty
        if (this.size == 1) {
            //add null into index 0
            this.heap.add(null); //adds 1 to heap.size()
            this.heap.add(1, t);
   //System.out.println("heap size after 1 insert: "+heap.size());
        } else {
            //add t as last element in heap
            this.heap.add(t);
            //System.out.println(this.heap);
            //bubble up t until heap property is satisfied
            //swap elements if child < parent
            int curr = this.size;
            T parent = this.heap.get(curr / 2);
            //System.out.println("for t: "+t+ "parent is: "+parent);
            while (curr != 1 && this.comp.compare(t, parent) < 0) {
                this.swap(curr, curr / 2);
                curr = curr / 2;
                parent = this.heap.get(curr / 2);
            } 
        }
    }

    
    /** Remove "best" value. This value is the "best" value in the
     *  queue as determined by the comparator for the queue.
     *  @return T
     *  @throws QueueEmptyException If queue is empty.
     */
    public T remove() throws QueueEmptyException {
        if (this.isEmpty()) {
            throw new QueueEmptyException();
        }
        //swap value to remove with last value
        this.swap(1, this.size);
      //remove last value
        T toRemove = this.heap.get(this.size);
        this.heap.remove(this.size);
        this.size--;
        //need to bubble new top value down 
        int curr = 1;
        int lchild = curr * 2;
        int rchild = curr * 2 + 1;
        //this is an infinite loop
        while ((curr <= (this.size) / 2) && curr != -1) {
            curr = this.bubbleDown(curr);
        } 
        return toRemove; 
    }
 
    
    /** Get the "best" value. This value is the "best" value in the
     *  queue as determined by the comparator for the queue.  [Note,
     *  "best" is at the root in a heap-based implementation.]
     *  @return best value in the queue.
     *  @throws QueueEmptyException If queue is empty.
     */
    public T peek() throws QueueEmptyException {
        if (this.isEmpty()) {
            throw new QueueEmptyException();
        }
        //System.out.println(this.heap);
        return this.heap.get(1);
    }

    /** Get the number of elements in the queue.
     *  @return the numbers
     */
    public int size() {
        return this.size;
    }

    /** No elements?
     *  @return True if queue is empty, false otherwise.
     */
    public boolean isEmpty() {
        return this.size == 0;
    }


    /** Dump the contents of the priority queue.
     */
    public void clear() {
        this.heap = new ArrayList<T>();
        this.size = 0;
    }
    
    /** Initialize a priority queue from a container of values.
     *  @param values the collection of starting values
     */
    public void init(Collection<T> values) {
        Iterator<T> iter = values.iterator();
        Object currObj;
        T curr;
        while (iter.hasNext()) {
            currObj = iter.next();
            curr = (T) currObj;
            this.insert(curr);
        }
    }

//--------------------------HELPER METHODS-----------------------//
    /**Method swaps data at indexA and indexB.
     * @param indexA first index
     * @param indexB second index
     */
    public void swap(int indexA, int indexB) {
        T dataA = this.heap.get(indexA);
        T dataB = this.heap.get(indexB);
        this.heap.set(indexA, dataB);
        this.heap.set(indexB, dataA);
    } 
    
   /** Method swaps data at parentindex with data at a child index.
    *  Swap only done if necessary to maintain heap property.
    *  returns index where data originally at parentIndex resides after swap.
    *  returns -1 if no swap occurs
    *  @param parentIndex index to try and swap
    *  @return int
    */
    public int bubbleDown(int parentIndex) {
        T parent = this.heap.get(parentIndex);
        T lchild = this.heap.get(parentIndex * 2);
        T minChild;
        int minChildIndex;

        //possible that there is no right child so need to check index
        if (this.size > parentIndex * 2) {
            T rchild = this.heap.get((parentIndex * 2) + 1);
            if (this.comp.compare(lchild, rchild) < 0) {
                minChild = lchild;
                minChildIndex = parentIndex * 2;
            } else {
                minChild = rchild;
                minChildIndex = (parentIndex * 2) + 1;
            }
        } else {
            minChild = lchild;
            minChildIndex = parentIndex * 2;
        }
        if (this.comp.compare(parent, minChild) > 0) {
            this.swap(parentIndex, minChildIndex);
            return minChildIndex;
        } 
        //if the parent is in the right place, send end signal
        return -1;

    }

} // end class def 
