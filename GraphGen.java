import java.util.List;

public interface GraphGen<G> {

    /** Get the number of edges. */
    int numEdges();

    /** Get the number of vertices. */
    int numVerts();

    /** Get the next ID to use in making a vertex. */
    int nextID();

    /** Create and add a vertex to the graph.
     *  @param d the data to store in the vertex
     *  @return true if successful, false otherwise
     */
    boolean addVertex(G d);

    /** Add a vertex if it doesn't exist yet. */
    boolean addVertex(GVertex<G> v);

    /** Add an edge, may also add the incident vertices. */
    boolean addEdge(WEdge<G> e);

    /** Add a (directed) edge, may also add the incident vertices. */
    boolean addEdge(GVertex<G> v, GVertex<G> u);

    /** Remove a (directed) edge if there.  */
    boolean deleteEdge(GVertex<G> v, GVertex<G> u);

    /** Return true if there is an edge between v and u. */
    boolean areAdjacent(GVertex<G> v, GVertex<G> u);

    /** Return a list of all the neighbors of vertex v.  */
    List<GVertex<G>> neighbors(GVertex<G> v);

    /** Return the number of edges incident to v.  */
    int degree(GVertex<G> v);

    /** Return true if v is an endpoint of edge e.  */
    boolean areIncident(WEdge<G> e, GVertex<G> v);

    /** Return a list of all the vertices that can be reached from v,
     * in the order in which they would be visited in a depth-first
     * search starting at v.  */
    List<GVertex<G>> depthFirst(GVertex<G> v);

    /** Return a list of all the edges.  */
    List<WEdge<G>> allEdges();

    /** Return a list of all the vertices.  */
    List<GVertex<G>> allVertices();

}
