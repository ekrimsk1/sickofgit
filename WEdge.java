/** Erez Krimsky, Gabriela Mizrahi-Arnaud, Ben Gellman
 * ekrimsk1, gmizrah1, bgellma1
 * Data Structrues Section 2
 * Project 4
 * Implementation of an edge class (for graphs), could be directed or not.
 * Uses generics.
 * @param <G> generic
 */
public class WEdge<G> implements Comparable<WEdge<G>> {

    /** Starting vertex of an edge. */
    private GVertex<G> source;
    /** Ending vertex of an edge. */
    private GVertex<G> end;
    /** Whether or not the edge is directed. */
    //MIGHT WANT TO EXCLUDE THIS
    private boolean directed;
    /** Weight of an edge. */
    private double weight;

    /** Create an undirected edge.
     *  @param u the start
     *  @param v the end
     *  @param w weight
     */
    public WEdge(GVertex<G> u, GVertex<G> v, double w) {
        this.source = u;
        this.end = v;
        this.weight = w;
        this.directed = false;
    }
    // can exclude this constructor if we only need undirected edges  
    /** Create an edge.
     *  @param u the start
     *  @param v the end
     *  @param dir true if directed, false otherwise
     */
    public WEdge(GVertex<G> u, GVertex<G> v, boolean dir) {
        this.source = u;
        this.end = v;
        this.directed = dir;
    }

    /** Is the edge directed.
     *  @return true if yes, false otherwise
     */
    public boolean isDirected() {
        return this.directed;
    }

    /** Is a vertex incident to this edge.
     *  @param v the vertex
     *  @return true if source or end, false otherwise
     */
    public boolean isIncident(GVertex<G> v) {
        return this.source.equals(v) || this.end.equals(v);
    }

    /** Get the starting endpoint vertex.
     *  @return the vertex
     */
    public GVertex<G> source() {
        return this.source;
    }

    /** Get the ending endpoint vertex.
     *  @return the vertex
     */
    public GVertex<G> end() {
        return this.end;
    }
    /** Get the weight of the edge.
     *  @return the weight
     */
    public double weight() {
        return this.weight;
    }
    /** Create a string representation of the edge.
     *  @return the string as (source,end,weight)
     */
    public String toString() {
        return "(" + this.source + "," + this.end + "," + this.weight + ")";
    }

    /** Check if two edges are the same.
     *  @param other the edge to compare to this
     *  @return true if directedness and endpoints match, false otherwise
     */
    public boolean equals(Object other) {
      //might not be able to use instance of on generics
        if (other instanceof WEdge) { 
 // I think the point of generics is to avoid this kind of casting 
            WEdge<G> e = (WEdge<G>) other;
            if (this.directed != e.directed) {
                return false;
            } 
          //can remove parts if only undirected maps
            if (this.directed) {
                return this.source.equals(e.source)
                    && this.end.equals(e.end);
            } else {
                return this.source.equals(e.source)
                    && this.end.equals(e.end)
                    || this.source.equals(e.end)
                    && this.end.equals(e.source);
            }
        }
        return false;
    }

    /** Make a hashCode based on the toString.
     *  @return the hashCode
     */
    public int hashCode() {
        return this.toString().hashCode();
    }
///////////COMPARABLE METHODS/////////
    /**Method compares edges based on weights.
     * @param other to compare to
     * @return 0 if equal, -1 if less than, 1 if greater than
     */
    public int compareTo(WEdge<G> other) {
        if (this.weight < other.weight) {
            return -1;
        } else if (this.weight > other.weight) {
            return 1;
        } else {
            return 0;
        }
    }
}
