
/**Gabriela Mizrahi-Arnaud, Ben Gellman and Erez Krimsky
 * gmizrah1, bgellma1 and ekrimsk1
 * Data Structures Section 2 
 * Project 4
 * Main driver class for Project 4 part C.
 */
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/** Class to convert an image to a graph and segment it. */
public final class P4C {

    /** Private empty constructor. */
    private P4C() {
    }

    /**
     * Convert an image to a graph of Pixels with edges between north, south,
     * east and west neighboring pixels.
     * 
     * @param image
     *            the image to convert
     * @param pd
     *            the distance object for pixels
     * @return the graph that was created
     */
    static WGraph<Pixel> imageToGraph(BufferedImage image, Distance<Pixel> pd) {
        long imToGraphStart = System.nanoTime();
        int h = image.getHeight();
        int w = image.getWidth();

        // add vertices to graph
        WGraphP4<Pixel> graph = new WGraphP4<Pixel>(1);
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                graph.addVertex(new Pixel(i, j, image.getRGB(i, j)));
            }
        }
        // add edges
        List<GVertex<Pixel>> verts = graph.allVertices();
        System.out.println("Number of Vertices: " + verts.size());
        // ArrayList<GVertex<Pixel>> verts = (ArrayList<GVertex<Pixel>>) verts1;
        GVertex<Pixel> tempv;
        for (int i = 0; i < verts.size(); i++) {
            tempv = verts.get(i);
            try {
                graph.addEdge(verts.get(i), verts.get(i + 1),
                     pd.distance(verts.get(i).data(), verts.get(i + 1).data()));
            } catch (IndexOutOfBoundsException e) {
            }

            try {
                graph.addEdge(verts.get(i), verts.get(i + w),
                     pd.distance(verts.get(i).data(), verts.get(i + w).data()));
            } catch (IndexOutOfBoundsException e) {
            }
        }
        System.out.println("Number of Edges: " + graph.numEdges());
        //System.out.println(System.nanoTime() - imToGraphStart);
        return graph;
    }

    /**
     * Return a list of edges in a minimum spanning forest by implementing
     * Kruskal's algorithm using fast union/finds.
     * 
     * @param g
     *            the graph to segment
     * @param kvalue
     *            the value to use for k in the merge test
     * @return a list of the edges in the minimum spanning forest
     */

    static Collection<List<GVertex<Pixel>>> segmenter(WGraph<Pixel> g, double kvalue) {
        PQHeap<WEdge<Pixel>> edgePQ = new PQHeap<WEdge<Pixel>>();
        
        edgePQ.init(g.allEdges());
       // System.out.println("num edges 1 " + edgePQ.size());
        List<WEdge<Pixel>> mstEdgeList = new ArrayList<>();
        if (g.numVerts() == 0) {
            //return mstEdgeList;
            return new LinkedList<List<GVertex<Pixel>>>();
        }

        Partition myPart = new Partition(g.numVerts());
        // set up hashamp to keep track of max & mins for each partition segment
        HashMap<Integer, MaxMin> segments = new HashMap<>();
        // add every vertex to the hashmap to initialize
        for (GVertex<Pixel> i : g.allVertices()) {
            segments.put(myPart.find(i.getGraphID()), 
                                 new MaxMin(i.data().getRGB()));
        }
        // System.out.println(segments.toString());

        WEdge<Pixel> temp;
        GVertex<Pixel> v;
        GVertex<Pixel> u;
        int pp = 0;
        while (!edgePQ.isEmpty()) {
            temp = edgePQ.remove();
            pp++;
            v = temp.source();
            u = temp.end();
            int vSegment = myPart.find(v.getGraphID());
            int uSegment = myPart.find(u.getGraphID());
            if (myPart.find(v.getGraphID()) != myPart.find(u.getGraphID())) {
                // System.out.println("Find v at " + v.id() + " :"
                // + myPart.find(v.id()));
                // System.out.println("Find u at " + u.id() + " :"
                // + myPart.find(u.id()));
                // System.out.println("added " + temp.toString());

                // check segment condition before merging
                MaxMin uMaxMin = segments.get(uSegment);
                MaxMin vMaxMin = segments.get(vSegment);
                int[] diffUV = diff(vMaxMin, uMaxMin);
                int[] minDiff = findMinDiff(uMaxMin, vMaxMin);
                int sizeV = vMaxMin.getSize();
                int sizeU = uMaxMin.getSize();
                if (checkParam(diffUV, minDiff, sizeV, sizeU, kvalue)) {
                    // add edge to edge list and union partition
                    mstEdgeList.add(temp);
                    myPart.union(u.getGraphID(), v.getGraphID());
                    // update info in hashmap- delete entry of segment that got
                    // removed, update MaxMin in newly unioned segment
                    int[] newMax = findMax(vMaxMin, uMaxMin);
                    int[] newMin = findMin(vMaxMin, uMaxMin);

                    // figure out which segment got deleted and update
                    // accordingly
                    if (myPart.find(v.getGraphID()) != vSegment) {
                        // this means v segment got deleted
                        segments.put(uSegment, new MaxMin(newMax, newMin, 
                                                              sizeV + sizeU));
                        segments.remove(vSegment);
                    } else {
                        // this means u got deleted
                        segments.put(vSegment, new MaxMin(newMax, newMin, 
                                                              sizeV + sizeU));
                        segments.remove(uSegment);
                    }

                }
                // System.out.println(myPart.toString());
                // System.out.println(Arrays.toString(array));
            }
        }
       // System.out.println("num edges 2 " + pp);
        //return mstEdgeList;
        HashMap<Integer, List<GVertex<Pixel>>> segMap = new HashMap<>();
        int[] parents = myPart.getParents();
        Integer tempNum;
        List<GVertex<Pixel>> tempList;
        for (int i = 0; i < parents.length; i++) {
            try{
                GVertex<Pixel> pix = segMap.get(parents[i]).get(0);
                tempNum = parents[pix.getGraphID()];
            } catch (NullPointerException e) {
                tempNum = null;
            }
            if (tempNum == null) {
                tempList = new LinkedList<GVertex<Pixel>>();
                tempList.add(g.allVertices().get(i));
                segMap.put(parents[i], tempList);
            } else {
                segMap.get(parents[i]).add(g.allVertices().get(i));
            }
        }


        Collection<List<GVertex<Pixel>>> vals = segMap.values();
        LinkedList<List<GVertex<Pixel>>> valsSansOne = new LinkedList<>();
        for (List<GVertex<Pixel>> x : vals) {
            if (x.size() > 1) {
                valsSansOne.add(x);
            }
        }
        return valsSansOne;


    }

    /**
     * Returns string representation of integer array.
     * @param max input integer array
     * @return String representation of the array
     */
    public static String arrayToString(int[] max) {
        String toString = " ";
        toString = toString + "(";
        for (int i = 0; i < max.length; i++) {
            toString = toString + max[i] + ",";
        }
        toString = toString + ")\n";
        return toString;
    }

    /**
     * Checks if an edge has the proper parameters to be added to the edge 
     * list for a. 
     * minimum spanning tree (or forest).
     * @param diffAB difference that would be found for the union of A and B.
     * @param minDiff minimum difference between the two segments 
     * @param sizeA size of segment A
     * @param sizeB size of segment B
     * @param k tuning parameter for segmentation 
     * @return True if valid edge, False otherwise.
     */
    public static boolean checkParam(int[] diffAB, int[] minDiff, 
                                    int sizeA, int sizeB, double k) {

        double compare = k / (sizeA + sizeB);
        for (int i = 0; i < diffAB.length; i++) {
            // System.out.println("diffAB: "+diffAB[i]+" minDiff: "+minDiff[i]);
            if (diffAB[i] > minDiff[i] + compare) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns elements wise maximum for two integer array arrays.
     * @param one first integer array
     * @param two second integer array
     * @return the element wise maximum
     */
    public static int[] findMax(MaxMin one, MaxMin two) {
        int[] oneMaxRGB = one.getMaxRGB();
        int[] twoMaxRGB = two.getMaxRGB();
        int[] max = new int[oneMaxRGB.length];
        for (int i = 0; i < max.length; i++) {
            if (oneMaxRGB[i] > twoMaxRGB[i]) {
                max[i] = oneMaxRGB[i];
            } else {
                max[i] = twoMaxRGB[i];
            }
        }
        return max;
    }

    /**
     * Returns elements wise minimum for two integer array arrays.
     * @param one first integer array
     * @param two second integer array
     * @return the element wise minimum
     */
    public static int[] findMin(MaxMin one, MaxMin two) {
        int[] oneMinRGB = one.getMinRGB();
        int[] twoMinRGB = two.getMinRGB();
        int[] min = new int[oneMinRGB.length];
        for (int i = 0; i < min.length; i++) {
            if (oneMinRGB[i] < twoMinRGB[i]) {
                min[i] = oneMinRGB[i];
            } else {
                min[i] = twoMinRGB[i];
            }
        }
        return min;
    }

    /**Method finds the minium of Diff(One) and Diff(Two).
     * Works by comparing each component individually.
     * @param one first MaxMin
     * @param two second MaxMin
     * @return int[]
     */
    public static int[] findMinDiff(MaxMin one, MaxMin two) {

        int[] oneDiff = one.diff();
        int[] twoDiff = two.diff();
        int[] minDiff = new int[oneDiff.length];
        for (int i = 0; i < minDiff.length; i++) {
            if (oneDiff[i] < twoDiff[i]) {
                minDiff[i] = oneDiff[i];
            } else {
                minDiff[i] = twoDiff[i];
            }
        }
        return minDiff;
    }
    /**Method finds Diff(A U B).
     * @param a first Max Min
     * @param b second MaxMin
     * @return int[]
     */
    public static int[] diff(MaxMin a, MaxMin b) {
        int[] aMax = a.getMaxRGB();
        int[] aMin = a.getMinRGB();
        int[] bMax = b.getMaxRGB();
        int[] bMin = b.getMinRGB();
        int[] diff = new int[aMax.length];
        int maxi;
        int mini;
        for (int i = 0; i < diff.length; i++) {
            maxi = Math.max(aMax[i], bMax[i]);
            mini = Math.min(aMin[i], bMin[i]);
            diff[i] = maxi - mini;
        }
        return diff;
    }
    /** Method takes in a list of WEdges and finds all trees in its forest.
     *  Returns a HashSet of HashSets where each outer HashSet is one connected
     *  component. Each inner HashSet is all the vertices in that component.
     *  @param edgs list of Edges
     *  @return HashSet<Hashset<GVertex<Pixel>>>
     */
    public static HashSet<HashSet<GVertex<Pixel>>> 
                                         getSegments(List<WEdge<Pixel>> edgs) {
        HashSet<HashSet<GVertex<Pixel>>> segments = 
                                        new HashSet<HashSet<GVertex<Pixel>>>();
        WGraphP4<Pixel> segGraph = new WGraphP4<Pixel>();
        Iterator<WEdge<Pixel>> iter = edgs.iterator();
        while (iter.hasNext()) {
            segGraph.addEdge(iter.next());
        }
        List<GVertex<Pixel>> verts = segGraph.allVertices();
        Iterator<GVertex<Pixel>> iter2 = verts.iterator();
        Iterator<HashSet<GVertex<Pixel>>> segIter;
        List<GVertex<Pixel>> temp;
        HashSet<GVertex<Pixel>> tempHash;
        boolean found = false;
        GVertex<Pixel> tempV;
        
        while (iter2.hasNext()) {
            found = false;
            tempV = iter2.next();
            segIter = segments.iterator();
            while (segIter.hasNext()) {
                if (segIter.next().contains(tempV)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                temp = segGraph.depthFirst(tempV);
                tempHash = new HashSet<GVertex<Pixel>>();
                for (int i = 0; i < temp.size(); i++) {
                    tempHash.add(temp.get(i));
                }
            
                segments.add(tempHash);
            }
 
        }
        return segments;
    }

    /**Main method to intake image as command line arg.
     * Creates image file of segments.
     * @param args commandline arguments
     */
    public static void main(String[] args) {
        /**Hexcode for gray.*/
        final int gray = 0x202020;

        try {
          // the line that reads the image file

            BufferedImage image = ImageIO.read(new File(args[0]));
            WGraph<Pixel> g = imageToGraph(image, new PixelDistance());
            
            //List<WEdge<Pixel>> res = segmenter(g, Double.parseDouble(args[1]));
            Collection<List<GVertex<Pixel>>> res = segmenter(g, Double.parseDouble(args[1]));


            System.out.print("result =  " + res.size() + "\n");
            System.out.print("NSegments =  "
                            + (g.numVerts() - res.size()) + "\n");



            //HashSet<HashSet<GVertex<Pixel>>> segments = getSegments(res);
            //System.out.println("SIZE: " + segments.size());


            // make a background image to put a segment into
            for (int i = 0; i < image.getHeight(); i++) {
                for (int j = 0; j < image.getWidth(); j++) {
                    image.setRGB(j, i, gray);
                }
            }

            // After you have a spanning tree connected component x, 
            // you can generate an output image like this:
            
            //Iterator<HashSet<GVertex<Pixel>>> iter = segments.iterator();
            Iterator<List<GVertex<Pixel>>> iter = res.iterator();
            //HashSet<GVertex<Pixel>> segment;
            List<GVertex<Pixel>> segment;
            String filename = args[0];
            int periodIndex = filename.indexOf(".");
            String filenameShort = filename.substring(0, periodIndex);
            String suffix = filename.substring(periodIndex, filename.length());
            int count = 1;
            while (iter.hasNext()) {
                segment = iter.next();
                for (GVertex<Pixel> i: segment)  {
                    Pixel d = i.data();
                    image.setRGB(d.row(), d.col(), d.value());
                }

                File f = new File(filenameShort + count + suffix);
                count++;
                ImageIO.write(image, "png", f);
                for (int i = 0; i < image.getHeight(); i++) {
                    for (int j = 0; j < image.getWidth(); j++) {
                        image.setRGB(j, i, gray);
                    }
                }
                // You'll need to do that for each connected component,
                // writing each one to a different file, clearing the
                // image buffer first
                // 

            } 
            
        } catch (IOException e) {
            System.out.print("Missing File!\n");
                   // log the exception, // re-throw if desired 
        }
        
    } // end main 
}
