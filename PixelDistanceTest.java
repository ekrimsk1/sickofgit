import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.Comparator;
import java.lang.Byte;

public class PixelDistanceTest {

    public static Pixel a;
    public static Pixel b;
    public static PixelDistance pd = new PixelDistance();
    @Test
    public void testPixelDistance() {
        a = new Pixel(0,0, 0x8070F09);
System.out.println("a red: "+a.getRed());
System.out.println("a green: "+a.getGreen());
System.out.println("a blue: "+a.getBlue());

        b = new Pixel(0, 1, 0x9080E0A);
 System.out.println("b red: "+b.getRed());
System.out.println("b green: "+b.getGreen());
System.out.println("b blue: "+b.getBlue());

       assertEquals(3, pd.distance(a, b), 0);
    }    
   
    }


