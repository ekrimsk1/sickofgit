/** Erez Krimsky, Gabriela Mizrahi-Arnaud, Ben Gellman
 * ekrimsk1, gmizrah1, bgellma1
 * Data Structrues Section 2
 * Project 4
 * Class to represent a vertex (in a graph).
 * Using generics.
 * @param <G> generic
 */
public class GVertex<G> implements Comparable<GVertex<G>> {

    /* Note that the nextID variable had to be moved to the graph class. */

    /** Vertex unique ID number. */
    private int num;

    /** Data stored in the vertex. */
    private G data;

    /**The ID in teh context of the graph this vertex is in.*/
    private int graphID;

    /** Create a new vertex.
     *  @param d the data to store in the node
     *  @param id the unique id of the node
     */
    public GVertex(G d, int id) {
        this.data = d;
        this.num = id;
        this.graphID = -1;
    }

    /**Set graphID when put into graph.
     * @param i the id
     */
    public void setGraphID(int i) {
        this.graphID = i;
    }

    /**Get the graphID of this vertex.
     * @return the graphID
     */
    public int getGraphID() {
        return this.graphID;
    }

    /** Get the id of this vertex.
     *  @return the id
     */
    public int id() {
        return this.num;
    }

    /**Get the data of this vertex.
     * @return the data
     */
    public G data() {
        return this.data;
    }

    /** Get a string representation of the vertex.
     *  @return the string 
     */
    public String toString() {
        return this.num + "";
    }

    /** Check if two vertices are the same based on ID.
     *  @param other the vertex to compare to this
     *  @return true if the same, false otherwise
     */
    public boolean equals(Object other) {
        if (other instanceof GVertex) {
            GVertex v = (GVertex) other;
            return this.num == v.num;  // want these to be unique
        }
        return false;
    }

    /** Get the hashcode of a vertex based on its ID.
     *  @return the hashcode
     */
    public int hashCode() {
        return (new Integer(this.num)).hashCode();
    }

    /** Compare two vertices based on their IDs.
     *  @param other the vertex to compare to this
     *  @return negative if this < other, 0 if equal, positive if this > other
     */
    public int compareTo(GVertex<G> other) {
        return this.num - other.num;
    }
}
