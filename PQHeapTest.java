/** Tests for MaxPriorityQueue interface.
 * Gaby Mizrahi and Ben Gellman and Erez Krimsky
*gmizrah1 and bgellma1 ekrimsk1
*600.226.02
*P4
*Tests for Priority Queue Heap
*/

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.Comparator;

import java.util.ArrayList;

public class PQHeapTest{
    static PQHeap<Integer> intpq;
    static PQHeap<String> strpq;
    static PQHeap<Integer> intpqComp;
    static PQHeap<String> strpqComp;

    static Integer[] intra = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    static String[] strra = {"1", "2", "3", "4", "5", "6", "7", "8", "9","10"};
    static Integer[] intraRand = {3, 4, 1, 7, 8, 2, 10, 6, 5, 9};
    static Comparator comp = new Comparator<Integer>() {
        public int compare(Integer t1, Integer t2) {
            return t1.compareTo(t2);
        }
    };
    
    //helper method to fill intpq
    public static PQHeap<Integer> fillIntpq() {
         PQHeap<Integer> newIntpq = new PQHeap<Integer>();
         for(int i = 1; i <= intra.length; i ++) {
            newIntpq.insert(intra[i-1]);
        }
        return newIntpq;
    }
    //helper method to fill strpq
    public static PQHeap<String> fillStrpq() {
         PQHeap<String> newStrpq = new PQHeap<String>();
         for(int i = 1; i <= strra.length; i ++) {
            newStrpq.insert(strra[i-1]);
        }
        return newStrpq;
    }
    //helper method to fil intpq randomly
    public static PQHeap<Integer> fillIntpqRand() {
        PQHeap<Integer> newIntpq = new PQHeap<Integer>();
        for (int i = 0; i < intraRand.length; i++) {
            newIntpq.insert(intraRand[i]);
        }
        return newIntpq; 
    }
    //helper method to get arraylist for collections for init
    public static ArrayList<Integer> getInts(Integer[] inra) {
        ArrayList<Integer> ra = new ArrayList<Integer>();
        for (int i = 0; i < inra.length; i++) {
            ra.add(inra[i]);
        }
        return ra;
    }

    //MEthods to fill comparator specified pqheaps
    //
    //helper method to fill intpqComp
    public static PQHeap<Integer> fillIntpqComp() {
         PQHeap<Integer> newIntpqComp = new PQHeap<Integer>();
         for(int i = 1; i <= intra.length; i ++) {
            newIntpqComp.insert(intra[i-1]);
        }
        return newIntpqComp;
    }
    //helper method to fill strpqComp
    public static PQHeap<String> fillStrpqComp() {
         PQHeap<String> newStrpqComp = new PQHeap<String>();
         for(int i = 1; i <= strra.length; i ++) {
            newStrpqComp.insert(strra[i-1]);
        }
        return newStrpqComp;
    }
    //helper method to fil intpqComp randomly
    public static PQHeap<Integer> fillIntpqCompRand() {
        PQHeap<Integer> newIntpqComp = new PQHeap<Integer>();
        for (int i = 0; i < intraRand.length; i++) {
            newIntpqComp.insert(intraRand[i]);
        }
        return newIntpqComp; 
    }


    public static ArrayList<String> getStrings(String[] inra) {
        ArrayList<String> ra = new ArrayList<String>();
        for (int i = 0; i < inra.length; i++) {
            ra.add(inra[i]);
        }
        return ra;
    }
    
    @BeforeClass
    public static void init() {
        intpq = new PQHeap<Integer>();
        strpq = new PQHeap<String>();
        intpqComp = new PQHeap<Integer>(comp);
        strpqComp = new PQHeap<String>(comp);

    }

    @Before
    public void clearAll() {
       intpq.clear();
       strpq.clear();
       intpqComp.clear();
       strpqComp.clear();
    }
    
    @Test 
    public void testSizeOnEmpty() {
        assertEquals(0, intpq.size());
        assertEquals(0, strpq.size());
    }
    @Test
    public void testSizeAfterOneAdd() {
        //this test is dependent on insert working
        intpq.insert(5); 
        assertEquals(1, intpq.size());
        strpq.insert("5");
        assertEquals(1, strpq.size());
    }
    @Test
    public void testPeekafterOneInsert() {
        intpq.insert(5);
        assertEquals((Integer)5, intpq.peek());
        strpq.insert("5");
        assertEquals("5", strpq.peek());
    }

    @Test
    public void testSizeAfterOneRemove() {
        //this test is dependent on delete working
        intpq = fillIntpq();
        intpq.remove();
        assertEquals(intra.length-1, intpq.size());
        
        strpq = fillStrpq(); 
        strpq.remove();
        assertEquals(strra.length-1, strpq.size());
    }
    @Test
    public void testSizeAfterManyRemoves() {
       intpq = fillIntpq();
       for (int i = 0; i < intra.length; i++) {
            intpq.remove();
            assertEquals(intra.length-i-1, intpq.size());
       }
     
       strpq = fillStrpq();
       for (int i = 0; i < strra.length; i++) {
            strpq.remove();
            assertEquals(strra.length-i-1, strpq.size());
        } 
    }
    @Test
    public void testSizeAfterClear() {
        //this test relies on clear working
        intpq = fillIntpq();
        intpq.clear();
        assertEquals(0, intpq.size());
      
        strpq = fillStrpq();
        strpq.clear();
        assertEquals(0, strpq.size());
    }
    @Test
    public void testIsEmptyOnEmptyPQ() {
        assertTrue(intpq.isEmpty());
        assertTrue(strpq.isEmpty());
    }
    @Test
    public void testIsEmptyAfterOneAdd() {
        intpq.insert(1);
        assertFalse(intpq.isEmpty());
        strpq.insert("1");
        assertFalse(strpq.isEmpty());
    }
    @Test
    public void testIsEmptyAfterManyAdds() {
        intpq = fillIntpq();
        assertFalse(intpq.isEmpty());
        strpq = fillStrpq();
        assertFalse(strpq.isEmpty());
    }
    @Test 
    public void testIsEmptyAfterRemoveAll() {
        intpq = fillIntpq();
        for (int i = 0; i < intra.length; i ++) {
            intpq.remove();
        }
        assertTrue(intpq.isEmpty());
    
        strpq = fillStrpq();
        for (int i = 0; i < strra.length; i++) {
            strpq.remove();
        }
        assertTrue(strpq.isEmpty());
    }
    @Test
    public void testIsEmptyAfterClear() {
        intpq = fillIntpq();
        intpq.clear();
        assertTrue(intpq.isEmpty());
       
        strpq = fillStrpq();
        strpq.clear();
        assertTrue(strpq.isEmpty());
    }
    @Test (expected = QueueEmptyException.class)
        public void testRemoveIfEmpty() {
        //assertTrue(intpq.isEmpty());
            intpq.remove();
    }

    @Test (expected = QueueEmptyException.class)
        public void testPeekIfEmpty() {
            intpq.peek();
        }
    @Test (expected = QueueEmptyException.class)
        public void testRemoveIfEmptyStr() {
        //assertTrue(intpq.isEmpty());
            strpq.remove();
    }


    @Test
    public void testClearOnEmpty() {
        intpq.clear();
        assertEquals(0, intpq.size());
        assertTrue(intpq.isEmpty());
        strpq.clear();
        assertEquals(0, strpq.size());
        assertTrue(strpq.isEmpty());
    }
    @Test
    public void testClearOnFull() {
        intpq = fillIntpq();
        intpq.clear();
        assertEquals(0, intpq.size());
    
        strpq = fillStrpq();
        strpq.clear();
        assertEquals(0, strpq.size());
    }
    @Test
    public void clearAfterClear() {
        intpq = fillIntpq();
        intpq.clear();
        assertEquals(0, intpq.size());
        intpq.clear();
        assertEquals(0, intpq.size());
  
        strpq = fillStrpq();
        strpq.clear();
        assertEquals(0, strpq.size());
        strpq.clear();
        assertEquals(0, strpq.size());
    }
    @Test (expected = QueueEmptyException.class)
        public void peekAfterClearInt() {
            intpq = fillIntpq();
            intpq.clear();
            intpq.peek();
        }

    @Test (expected = QueueEmptyException.class)
        public void removeAfterClearInt() {
            intpq = fillIntpq();
            intpq.clear();
            intpq.remove();
        }

    @Test (expected = QueueEmptyException.class)
        public void peekAfterClearStr() {
            strpq = fillStrpq();
            strpq.clear();
            strpq.peek();
        }

    @Test (expected = QueueEmptyException.class)
        public void removeAfterClearStr() {
            strpq = fillStrpq();
            strpq.clear();
            strpq.remove();
        }
    @Test 
    public void peekAfterOneInsert() { 
        intpq.insert(5);
        assertEquals((Integer)5, intpq.peek());
        strpq.insert("5");
        assertEquals("5", strpq.peek());
    }
    @Test
    public void peekAfterManyInsert() {
        for (int i = 0; i < intra.length; i ++) {
            intpq.insert(intra[i]);
            assertEquals((Integer)1, intpq.peek());
        }
        for (Integer i = 0; i < strra.length; i ++) {
            strpq.insert(strra[i]);
            assertEquals("1", strpq.peek());
        }
    }
    @Test
    public void peekAfterRandomInserts() {
        intpq.insert(3);
        assertEquals((Integer) 3, intpq.peek());
        intpq.insert(4);
        assertEquals((Integer) 3, intpq.peek());
        intpq.insert(1);
        assertEquals((Integer) 1, intpq.peek());
        intpq.insert(7);
        assertEquals((Integer) 1, intpq.peek());
        intpq.insert(8);
        assertEquals((Integer) 1, intpq.peek());
        intpq.insert(2);
        assertEquals((Integer) 1, intpq.peek());
        intpq.insert(10);
        assertEquals((Integer) 1, intpq.peek());
        intpq.insert(6);
        assertEquals((Integer) 1, intpq.peek());
        intpq.insert(5);
        assertEquals((Integer) 1, intpq.peek());
        intpq.insert(9);
        assertEquals((Integer) 1, intpq.peek());
         
        strpq.insert("3");
        assertEquals("3", strpq.peek());
        strpq.insert("4");
        assertEquals("3", strpq.peek());
        strpq.insert("1");
        assertEquals("1", strpq.peek());
        strpq.insert("7");
        assertEquals("1", strpq.peek());
        strpq.insert("8");
        assertEquals("1", strpq.peek());
        strpq.insert("2");
        assertEquals("1", strpq.peek());
        strpq.insert("10");
        assertEquals("1", strpq.peek());
        strpq.insert("6");
        assertEquals("1", strpq.peek());
        strpq.insert("5");
        assertEquals("1", strpq.peek());
        strpq.insert("9");
        assertEquals("1", strpq.peek());
    }
    @Test 
    public void removeAfterOneInsert() { 
        intpq.insert(5);
        assertEquals((Integer)5, intpq.remove());
        strpq.insert("5");
        assertEquals("5", strpq.remove());
    }
    @Test
    public void removeAfterManyInsert() {
        for (int i = 0; i < intra.length; i ++) {
            intpq.insert(intra[i]);
            assertEquals((Integer)(i + 1), intpq.remove());
        }
        for (Integer i = 0; i < strra.length; i ++) {
            strpq.insert(strra[i]);
            Integer toRemove = i + 1;
            assertEquals(toRemove.toString(), strpq.remove());
        }
    }
    @Test
    public void testRemoveOnFullPQ() {
        intpq = fillIntpqRand();
        for (int i = 1; i <= intpq.size(); i++) {
            assertEquals((Integer)i,intpq.remove());
        }
            
    }
    @Test
    public void removeAfterRandomInserts() {
        intpq.insert(3);
        assertEquals((Integer) 3, intpq.remove());
        intpq.insert(4);
        assertEquals((Integer) 4, intpq.remove());
        intpq.insert(1);
        assertEquals((Integer) 1, intpq.remove());
        intpq.insert(7);
        assertEquals((Integer) 7, intpq.remove());
        intpq.insert(8);
        assertEquals((Integer) 8, intpq.remove());
        intpq.insert(2);
        assertEquals((Integer) 2, intpq.remove());
        intpq.insert(10);
        assertEquals((Integer) 10, intpq.remove());
        intpq.insert(6);
        assertEquals((Integer) 6, intpq.remove());
        intpq.insert(5);
        assertEquals((Integer) 5, intpq.remove());
        intpq.insert(9);
        assertEquals((Integer) 9, intpq.remove());
         
        strpq.insert("3");
        assertEquals("3", strpq.remove());
        strpq.insert("4");
        assertEquals("4", strpq.remove());
        strpq.insert("1");
        assertEquals("1", strpq.remove());
        strpq.insert("7");
        assertEquals("7", strpq.remove());
        strpq.insert("8");
        assertEquals("8", strpq.remove());
        strpq.insert("2");
        assertEquals("2", strpq.remove());
        strpq.insert("10");
        assertEquals("10", strpq.remove());
        strpq.insert("6");
        assertEquals("6", strpq.remove());
        strpq.insert("5");
        assertEquals("5", strpq.remove());
        strpq.insert("9");
        assertEquals("9", strpq.remove());
    }
    @Test
    public void removeAfterRandomInsert() {
        intpq = fillIntpqRand();
        for(Integer i = 1; i <= intra.length; i++) {
            assertEquals(i, intpq.remove());
        }
    } 
    @Test
    public void removeAllAtOnceAfterInOrderInserts() {
        for (int i = 0; i < intra.length; i ++) {
            intpq.insert(intra[i]);
            assertEquals((Integer)(i + 1), intpq.remove());
        }

        intpq = fillIntpq();
        for (int i = 1; i <= intra.length; i++) {
            assertEquals((Integer) intra[i-1], intpq.remove());
        }
        
        String[] ra = {"1", "10", "2", "3", "4", "5", "6", "7", "8", "9"};
        for (Integer i = 1; i <= strra.length; i ++) {
            strpq.insert(strra[i-1]);
            assertEquals(i.toString(), strpq.remove());
        }
        strpq = fillStrpq();

        for (int j = 1; j <= strra.length; j++) {
            assertEquals(ra[j-1], strpq.remove());
        }
    }
    @Test
    public void removeAllAfterNotInOrderInserts() {
        for (int i = 0; i < intraRand.length; i ++) {
            intpq.insert(intraRand[i]);
            assertEquals((Integer) intraRand[i], intpq.remove());
        }
        intpq.clear();
        for (int i = 0; i < intraRand.length; i ++) {
            intpq.insert(intraRand[i]);
        }
            

        for (int i = 1; i <= intra.length; i++) {
            assertEquals((Integer) i, intpq.remove());
        }
    }
    @Test
    public void removeDuplicates() {
        intpq = fillIntpq();
        for (int i = 0; i < intraRand.length; i ++) {
            intpq.insert(intraRand[i]);
        }
//not sure how to get this to work with our queue /*
       /* for (int i = 0; i < inptq.size(); i++) {
            assertEquals(i, intpq.remove());
        }*/

    }
    
    @Test (expected = QueueEmptyException.class)
        public void exceptionAfterOneRemoveInt() {
            intpq.insert(intra[0]);
            Integer j = intpq.remove();
            intpq.remove();
        }
    @Test (expected = QueueEmptyException.class)
        public void exceptionAfterOneRemoveStr() {
            strpq.insert(strra[0]);
            String j = strpq.remove();
            strpq.remove();
        }

    //Insert tests
    @Test
    public void testOneInsert() {
        intpq.insert(6);
        strpq.insert("6");
        assertEquals(1, intpq.size());
        assertEquals(1, strpq.size());
        
        assertEquals((Integer) 6, intpq.peek());
        assertEquals((Integer) 6, intpq.remove());
        assertEquals("6", strpq.peek());
        assertEquals("6", strpq.remove());
        assertTrue(intpq.isEmpty());
        assertTrue(strpq.isEmpty());
    }

    @Test
    public void insertDuplicatesSize() {
        intpq = fillIntpq();
        for (int i = 0; i < intraRand.length; i ++) {
            intpq.insert(intraRand[i]);
            assertEquals(intpq.size(), i + 11);
        }
    }
    @Test
    public void insertDuplicatesPeek() {
        intpq = fillIntpq();
        for (int i = 0; i < intraRand.length; i ++) {
            intpq.insert(intraRand[i]);
            assertEquals((Integer)1, intpq.peek());
        }
    }



    //init tests
    @Test
    public void testInitSize() {
        ArrayList<Integer> intraList = getInts(intra);
        ArrayList<String> strraList = getStrings(strra);
        ArrayList<Integer> intraRandList = getInts(intraRand);
        intpq.init(intraList);
        strpq.init(strraList);
        assertFalse(intpq.isEmpty());
        assertFalse(strpq.isEmpty());
        assertEquals(intra.length, intpq.size());
        assertEquals(strra.length, strpq.size());
    }
    
    @Test
    public void testInitValues() {
        ArrayList<Integer> intraList = getInts(intra);
        ArrayList<String> strraList = getStrings(strra);
        ArrayList<Integer> intraRandList = getInts(intraRand);
        strraList.remove("10");
        intpq.init(intraList);
        strpq.init(strraList);
        String iStr;
        Integer j;
        for (int i = 1; i <= intra.length; i ++) {
            assertEquals((Integer) i, intpq.remove());
        }
        for (int i = 1; i < strraList.size(); i++) {
            j = (Integer) i;
            iStr = j.toString();
            assertEquals(iStr, strpq.remove());
        }
        intpq.clear();
        intpq.init(intraRandList);
        for (int i = 1; i <= intraRand.length; i ++) {
            assertEquals((Integer) i, intpq.remove());
        }
    }


    //TEST FOR COMPARATOR CONSTRUCTOR

 @Test 
    public void testSizeOnEmptyComp() {
        assertEquals(0, intpqComp.size());
        assertEquals(0, strpq.size());
    }
    @Test
    public void testSizeAfterOneAddComp() {
        //this test is dependent on insert working
        intpqComp.insert(5); 
        assertEquals(1, intpqComp.size());
        strpq.insert("5");
        assertEquals(1, strpq.size());
    }
    @Test
    public void testPeekafterOneInsertComp() {
        intpqComp.insert(5);
        assertEquals((Integer)5, intpqComp.peek());
        strpq.insert("5");
        assertEquals("5", strpq.peek());
    }
    @Test
    public void testSizeAfterManyAddsComp() {
        for(int i = 1; i <= intra.length; i ++) {
            intpqComp.insert(intra[i-1]);
            assertEquals(i, intpqComp.size());
        }
        for(int i = 1; i<= strra.length; i ++) {
            strpq.insert(strra[i-1]);
            assertEquals(i, strpq.size());
        }
    }
    @Test
    public void testPeekAfterManyAddsComp() {
        for(int i = 1; i <= intra.length; i ++) {
            intpqComp.insert(intra[i-1]);
            assertEquals((Integer)1, intpqComp.peek());
        }
        for(int i = 1; i<= strra.length; i ++) {
            strpq.insert(strra[i-1]);
            assertEquals("1", strpq.peek());
        }
    }


    @Test
    public void testSizeAfterOneRemoveComp() {
        //this test is dependent on delete working
        intpqComp = fillIntpqComp();
        intpqComp.remove();
        assertEquals(intra.length-1, intpqComp.size());
        
        strpq = fillStrpq(); 
        strpq.remove();
        assertEquals(strra.length-1, strpq.size());
    }
    @Test
    public void testSizeAfterManyRemovesComp() {
       intpqComp = fillIntpqComp();
       for (int i = 0; i < intra.length; i++) {
            intpqComp.remove();
            assertEquals(intra.length-i-1, intpqComp.size());
       }
     
       strpq = fillStrpq();
       for (int i = 0; i < strra.length; i++) {
            strpq.remove();
            assertEquals(strra.length-i-1, strpq.size());
        } 
    }
    @Test
    public void testSizeAfterClearComp() {
        //this test relies on clear working
        intpqComp = fillIntpqComp();
        intpqComp.clear();
        assertEquals(0, intpqComp.size());
      
        strpq = fillStrpq();
        strpq.clear();
        assertEquals(0, strpq.size());
    }
    @Test
    public void testIsEmptyOnEmptyPQComp() {
        assertTrue(intpqComp.isEmpty());
        assertTrue(strpq.isEmpty());
    }
    @Test
    public void testIsEmptyAfterOneAddComp() {
        intpqComp.insert(1);
        assertFalse(intpqComp.isEmpty());
        strpq.insert("1");
        assertFalse(strpq.isEmpty());
    }
    @Test
    public void testIsEmptyAfterManyAddsComp() {
        intpqComp = fillIntpqComp();
        assertFalse(intpqComp.isEmpty());
        strpq = fillStrpq();
        assertFalse(strpq.isEmpty());
    }
    @Test 
    public void testIsEmptyAfterRemoveAllComp() {
        intpqComp = fillIntpqComp();
        for (int i = 0; i < intra.length; i ++) {
            intpqComp.remove();
        }
        assertTrue(intpqComp.isEmpty());
    
        strpq = fillStrpq();
        for (int i = 0; i < strra.length; i++) {
            strpq.remove();
        }
        assertTrue(strpq.isEmpty());
    }
    @Test
    public void testIsEmptyAfterClearComp() {
        intpqComp = fillIntpqComp();
        intpqComp.clear();
        assertTrue(intpqComp.isEmpty());
       
        strpq = fillStrpq();
        strpq.clear();
        assertTrue(strpq.isEmpty());
    }
    @Test (expected = QueueEmptyException.class)
        public void testRemoveIfEmptyComp() {
        //assertTrue(intpqComp.isEmpty());
            intpqComp.remove();
    }

    @Test (expected = QueueEmptyException.class)
        public void testPeekIfEmptyComp() {
            intpqComp.peek();
        }
    @Test (expected = QueueEmptyException.class)
        public void testRemoveIfEmptyStrComp() {
        //assertTrue(intpqComp.isEmpty());
            strpq.remove();
    }


    @Test
    public void testClearOnEmptyComp() {
        intpqComp.clear();
        assertEquals(0, intpqComp.size());
        assertTrue(intpqComp.isEmpty());
        strpq.clear();
        assertEquals(0, strpq.size());
        assertTrue(strpq.isEmpty());
    }
    @Test
    public void testClearOnFullComp() {
        intpqComp = fillIntpqComp();
        intpqComp.clear();
        assertEquals(0, intpqComp.size());
    
        strpq = fillStrpq();
        strpq.clear();
        assertEquals(0, strpq.size());
    }
    @Test
    public void clearAfterClearComp() {
        intpqComp = fillIntpqComp();
        intpqComp.clear();
        assertEquals(0, intpqComp.size());
        intpqComp.clear();
        assertEquals(0, intpqComp.size());
  
        strpq = fillStrpq();
        strpq.clear();
        assertEquals(0, strpq.size());
        strpq.clear();
        assertEquals(0, strpq.size());
    }
    @Test (expected = QueueEmptyException.class)
        public void peekAfterClearIntComp() {
            intpqComp = fillIntpqComp();
            intpqComp.clear();
            intpqComp.peek();
        }

    @Test (expected = QueueEmptyException.class)
        public void removeAfterClearIntComp() {
            intpqComp = fillIntpqComp();
            intpqComp.clear();
            intpqComp.remove();
        }

    @Test (expected = QueueEmptyException.class)
        public void peekAfterClearStrComp() {
            strpq = fillStrpq();
            strpq.clear();
            strpq.peek();
        }

    @Test (expected = QueueEmptyException.class)
        public void removeAfterClearStrComp() {
            strpq = fillStrpq();
            strpq.clear();
            strpq.remove();
        }
    @Test 
    public void peekAfterOneInsertComp() { 
        intpqComp.insert(5);
        assertEquals((Integer)5, intpqComp.peek());
        strpq.insert("5");
        assertEquals("5", strpq.peek());
    }
    @Test
    public void peekAfterManyInsertComp() {
        for (int i = 0; i < intra.length; i ++) {
            intpqComp.insert(intra[i]);
            assertEquals((Integer)1, intpqComp.peek());
        }
        for (Integer i = 0; i < strra.length; i ++) {
            strpq.insert(strra[i]);
            assertEquals("1", strpq.peek());
        }
    }
    @Test
    public void peekAfterRandomInsertsComp() {
        intpqComp.insert(3);
        assertEquals((Integer) 3, intpqComp.peek());
        intpqComp.insert(4);
        assertEquals((Integer) 3, intpqComp.peek());
        intpqComp.insert(1);
        assertEquals((Integer) 1, intpqComp.peek());
        intpqComp.insert(7);
        assertEquals((Integer) 1, intpqComp.peek());
        intpqComp.insert(8);
        assertEquals((Integer) 1, intpqComp.peek());
        intpqComp.insert(2);
        assertEquals((Integer) 1, intpqComp.peek());
        intpqComp.insert(10);
        assertEquals((Integer) 1, intpqComp.peek());
        intpqComp.insert(6);
        assertEquals((Integer) 1, intpqComp.peek());
        intpqComp.insert(5);
        assertEquals((Integer) 1, intpqComp.peek());
        intpqComp.insert(9);
        assertEquals((Integer) 1, intpqComp.peek());
         
        strpq.insert("3");
        assertEquals("3", strpq.peek());
        strpq.insert("4");
        assertEquals("3", strpq.peek());
        strpq.insert("1");
        assertEquals("1", strpq.peek());
        strpq.insert("7");
        assertEquals("1", strpq.peek());
        strpq.insert("8");
        assertEquals("1", strpq.peek());
        strpq.insert("2");
        assertEquals("1", strpq.peek());
        strpq.insert("10");
        assertEquals("1", strpq.peek());
        strpq.insert("6");
        assertEquals("1", strpq.peek());
        strpq.insert("5");
        assertEquals("1", strpq.peek());
        strpq.insert("9");
        assertEquals("1", strpq.peek());
    }
    @Test
    public void removeAfterRandomInsertsComp() {
        intpqComp.insert(3);
        assertEquals((Integer) 3, intpqComp.remove());
        intpqComp.insert(4);
        assertEquals((Integer) 4, intpqComp.remove());
        intpqComp.insert(1);
        assertEquals((Integer) 1, intpqComp.remove());
        intpqComp.insert(7);
        assertEquals((Integer) 7, intpqComp.remove());
        intpqComp.insert(8);
        assertEquals((Integer) 8, intpqComp.remove());
        intpqComp.insert(2);
        assertEquals((Integer) 2, intpqComp.remove());
        intpqComp.insert(10);
        assertEquals((Integer) 10, intpqComp.remove());
        intpqComp.insert(6);
        assertEquals((Integer) 6, intpqComp.remove());
        intpqComp.insert(5);
        assertEquals((Integer) 5, intpqComp.remove());
        intpqComp.insert(9);
        assertEquals((Integer) 9, intpqComp.remove());
         
        strpq.insert("3");
        assertEquals("3", strpq.remove());
        strpq.insert("4");
        assertEquals("4", strpq.remove());
        strpq.insert("1");
        assertEquals("1", strpq.remove());
        strpq.insert("7");
        assertEquals("7", strpq.remove());
        strpq.insert("8");
        assertEquals("8", strpq.remove());
        strpq.insert("2");
        assertEquals("2", strpq.remove());
        strpq.insert("10");
        assertEquals("10", strpq.remove());
        strpq.insert("6");
        assertEquals("6", strpq.remove());
        strpq.insert("5");
        assertEquals("5", strpq.remove());
        strpq.insert("9");
        assertEquals("9", strpq.remove());
    }

    @Test
    public void removeAllAtOnceAfterInOrderInsertsComp() {
        for (int i = 0; i < intra.length; i ++) {
            intpqComp.insert(intra[i]);
            assertEquals((Integer)(i + 1), intpqComp.remove());
        }

        intpqComp = fillIntpqComp();
        for (int i = 1; i <= intra.length; i++) {
            assertEquals((Integer) intra[i-1], intpqComp.remove());
        }
        
        String[] ra = {"1", "10", "2", "3", "4", "5", "6", "7", "8", "9"};
        for (Integer i = 1; i <= strra.length; i ++) {
            strpq.insert(strra[i-1]);
            assertEquals(i.toString(), strpq.remove());
        }
        strpq = fillStrpq();

        for (int j = 1; j <= strra.length; j++) {
            assertEquals(ra[j-1], strpq.remove());
        }
    }
    @Test
    public void removeAllAfterNotInOrderInsertsComp() {
        for (int i = 0; i < intraRand.length; i ++) {
            intpqComp.insert(intraRand[i]);
            assertEquals((Integer) intraRand[i], intpqComp.remove());
        }
        intpqComp.clear();
        for (int i = 0; i < intraRand.length; i ++) {
            intpqComp.insert(intraRand[i]);
        }
            

        for (int i = 1; i <= intra.length; i++) {
            assertEquals((Integer) i, intpqComp.remove());
        }
    }

    //=================================================
    @Test
    public void removeOnEmptyPQComp() {
      //FILL IN
      //how do we test for the exception?
    }
    @Test 
    public void removeAfterOneInsertComp() { 
        intpqComp.insert(5);
        assertEquals((Integer)5, intpqComp.remove());
        strpq.insert("5");
        assertEquals("5", strpq.remove());
    }
    @Test
    public void removeAfterManyInsertComp() {
        for (int i = 0; i < intra.length; i ++) {
            intpqComp.insert(intra[i]);
            assertEquals((Integer)(i + 1), intpqComp.remove());
        }
        for (Integer i = 0; i < strra.length; i ++) {
            strpq.insert(strra[i]);
            Integer toRemove = i + 1;
            assertEquals(toRemove.toString(), strpq.remove());
        }
    }
    @Test
    public void testRemoveOnFullPQComp() {
        intpqComp = fillIntpqCompRand();
       for (int i = 1; i <= intpqComp.size(); i++) {
            assertEquals((Integer)i,intpqComp.remove());
        }
            
    }
    @Test
    public void removeDuplicatesComp() {
        intpqComp = fillIntpqComp();
        for (int i = 0; i < intraRand.length; i ++) {
            intpqComp.insert(intraRand[i]);
        }
    }

    @Test (expected = QueueEmptyException.class)
        public void exceptionAfterOneRemoveIntComp() {
            intpqComp.insert(intra[0]);
            Integer j = intpqComp.remove();
            intpqComp.remove();
        }
    @Test (expected = QueueEmptyException.class)
        public void exceptionAfterOneRemoveStrComp() {
            strpq.insert(strra[0]);
            String j = strpq.remove();
            strpq.remove();
        }

    //Insert tests
    @Test
    public void testOneInsertComp() {
        intpqComp.insert(6);
        strpq.insert("6");
        assertEquals(1, intpqComp.size());
        assertEquals(1, strpq.size());
        
        assertEquals((Integer) 6, intpqComp.peek());
        assertEquals((Integer) 6, intpqComp.remove());
        assertEquals("6", strpq.peek());
        assertEquals("6", strpq.remove());
        assertTrue(intpqComp.isEmpty());
        assertTrue(strpq.isEmpty());
    }

    @Test
    public void insertDuplicatesSizeComp() {
        intpqComp = fillIntpqComp();
        for (int i = 0; i < intraRand.length; i ++) {
            intpqComp.insert(intraRand[i]);
            assertEquals(intpqComp.size(), i + 11);
        }
    }
    @Test
    public void insertDuplicatesPeekComp() {
        intpqComp = fillIntpqComp();
        for (int i = 0; i < intraRand.length; i ++) {
            intpqComp.insert(intraRand[i]);
            assertEquals((Integer)1, intpqComp.peek());
        }
    }
    //init tests
    @Test
    public void testInitSizeComp() {
        ArrayList<Integer> intraList = getInts(intra);
        ArrayList<String> strraList = getStrings(strra);
        ArrayList<Integer> intraRandList = getInts(intraRand);
        intpqComp.init(intraList);
        strpq.init(strraList);
        assertFalse(intpqComp.isEmpty());
        assertFalse(strpq.isEmpty());
        assertEquals(intra.length, intpqComp.size());
        assertEquals(strra.length, strpq.size());
    }
    
    @Test
    public void testInitValuesComp() {
        ArrayList<Integer> intraList = getInts(intra);
        ArrayList<String> strraList = getStrings(strra);
        ArrayList<Integer> intraRandList = getInts(intraRand);
        strraList.remove("10");
        intpqComp.init(intraList);
        strpq.init(strraList);
        String iStr;
        Integer j;
        for (int i = 1; i <= intra.length; i ++) {
            assertEquals((Integer) i, intpqComp.remove());
        }
        for (int i = 1; i < strraList.size(); i++) {
            j = (Integer) i;
            iStr = j.toString();
            assertEquals(iStr, strpq.remove());
        }
        intpqComp.clear();
        intpqComp.init(intraRandList);
        for (int i = 1; i <= intraRand.length; i ++) {
            assertEquals((Integer) i, intpqComp.remove());
        }
    }



} //end class
