/**Gabriela Mizrahi-Arnaud, Ben Gellman, Erez Krimsky
 * gmizrah1, bgelma1, ekrimsk1,
 * This class represents a pixel in an image.
 */

public class Pixel {
  
    /**8.*/
    static final int EIGHT = 8;
    /**16.*/
    static final int SIXT = 16;
    /**24.*/
    static final int TWENTFR = 24;
    /**Number to clear int.*/
    static final int CLEAR = 0xFF;
    /** The row a pixel is located in.*/
    int row;
    /** The column a pixel is located in.*/
    int col;
    /**Value:  first bye: empty, second byte:r, 3rd byte:g, 4th byte: blue.*/
    int value;

    /** Pixel Constructor.
     * @param myRow row of pixel
     * @param myCol column of pixel
     * @param val RGB value of pixel
     */
    public Pixel(int myRow, int myCol, int val) {
        this.row = myRow;
        this.col = myCol;
        this.value = val;
    }
   
    /**Returns red value of pixel.
    *  @return int
    */ 
    public int getRed() {
        int red = this.value >> SIXT;
        red = red & CLEAR;
        return red;
    }
    /**Returns green value of pixel.
     *@return int
     */
    public int getGreen() {
        int green = this.value >> EIGHT;
        green = green & CLEAR;
        return green;
    }
    /**Returns blue value of pixel.
     * @return int
     */
    public int getBlue() {
        int blue = this.value & CLEAR;
        return blue;
    }
    /** Returns rgb as an array.
    * @return int[]
    */
    public int[] getRGB() {
        int[] rGB = {this.getRed(), this.getGreen(), this.getBlue()};
        return rGB;    
    }
    
    /**
     * Get the column.
     * @return the column
     */
    public int col() {
        return this.col;
    }
    
    /**
     * Get the row.
     * @return the row
     */
    public int row() {
        return this.row;
    }
    
    /**
     * Get the value.
     * @return the value
     */
    public int value() {
        return this.value;
    }
    
    
     
}
