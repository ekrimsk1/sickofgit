/**
 * Erez Krimsky, Matthew Heacock
 * ekrimsk1, mheacoc1
 * 600.226 (2)
 * Written Assignment a4
 */

// a bunch of imports, not all are used 
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Test;
import java.util.LinkedList;

//Assume that an implementation called MaxPQHeap.java exists.

public class MaxPriorityQueueTest {

    // initialize the type for two MaxPriorityQueue to be tested
    static MaxPriorityQueue<Integer> intMPQ;
    static MaxPriorityQueue<String> stringMPQ;
    
    @Before // before each and every test 
    public void init() {
        intMPQ = new MaxPQHeap<>(); // assuming MaxPQHeap.java exists 
        stringMPQ = new MaxPQHeap<>();
    }
    
    
    //--------TESTING METHODS------------//
    
    // test size() method 
    @Test
    public void testSize() {
        assertEquals("size should be zero on initialization", 0, intMPQ.size());
        assertEquals("size should be zero on initialization", 0, stringMPQ.size());
        intMPQ.insert(1);
        stringMPQ.insert("one");
        assertEquals("size should be 1", 1, intMPQ.size());
        assertEquals("size should be 1", 1, stringMPQ.size());
        intMPQ.insert(2);
        stringMPQ.insert("two");
        assertEquals("size should be 2", 2, intMPQ.size());
        assertEquals("size should be 2", 2, stringMPQ.size());
        intMPQ.insert(3);
        stringMPQ.insert("three");
        assertEquals("size should be 3", 3, intMPQ.size());
        assertEquals("size should be 3", 3, stringMPQ.size());
        intMPQ.insert(8);
        stringMPQ.insert("eight");
        assertEquals("size should be 4", 4, intMPQ.size());
        assertEquals("size should be 4", 4, stringMPQ.size());
        
        intMPQ.clear();
        stringMPQ.clear();
        assertEquals("size should be 0 after clearing", 0, intMPQ.size());
        assertEquals("size should be 0 after clearing", 0, stringMPQ.size());
                                   
    }
    
    
    @Test //make sure duplicate values still increase the size 
    public void testDuplicatesIncreaseSize() {
        intMPQ.insert(1);
        stringMPQ.insert("one");
        intMPQ.insert(2);
        stringMPQ.insert("two");
        intMPQ.insert(3);
        stringMPQ.insert("three");
        intMPQ.insert(4);
        stringMPQ.insert("four");
        intMPQ.insert(5);
        stringMPQ.insert("five");
        intMPQ.insert(6);
        stringMPQ.insert("six");
        
        int i = intMPQ.size();
        int s = stringMPQ.size();
        
        intMPQ.insert(1);
        stringMPQ.insert("one");
        i++;
        s++;
        assertEquals("duplicates should increase size", i, intMPQ.size());
        assertEquals("duplicates should increase size", s, stringMPQ.size());
        
        intMPQ.insert(1);
        stringMPQ.insert("one");
        i++;
        s++;
        assertEquals("duplicates should increase size", i, intMPQ.size());
        assertEquals("duplicates should increase size", s, stringMPQ.size());
        
        intMPQ.insert(2);
        stringMPQ.insert("two");
        i++;
        s++;
        assertEquals("duplicates should increase size", i, intMPQ.size());
        assertEquals("duplicates should increase size", s, stringMPQ.size());      
    }
    
    //test isEmpty() method
    @Test
    public void testIsEmpty() {
        // make sure empty after initializing 
        assertTrue("should be empty after initialization", intMPQ.isEmpty());
        assertTrue("should be empty after initialization", stringMPQ.isEmpty());
        
        //make sure NOT empty after adding values 
        intMPQ.insert(1);
        stringMPQ.insert("one");
        assertFalse("should not be empty after inserting", intMPQ.isEmpty());
        assertFalse("should not be empty after inserting", stringMPQ.isEmpty());
        intMPQ.insert(2);
        stringMPQ.insert("two");
        intMPQ.insert(3);
        stringMPQ.insert("three");
        intMPQ.insert(4);
        stringMPQ.insert("four");
        intMPQ.insert(5);
        stringMPQ.insert("five");
        intMPQ.insert(6);
        stringMPQ.insert("six");
        assertFalse("should not be empty after inserting", intMPQ.isEmpty());
        assertFalse("should not be empty after inserting", stringMPQ.isEmpty());
        
        
        //make sure empty after clearing, redundant test
        intMPQ.clear();
        stringMPQ.clear();
        assertTrue("should be empty after clearing", intMPQ.isEmpty());
        assertTrue("should be empty after clearing", stringMPQ.isEmpty());
        
        // testing isEmpty after init with empty is in a test below!
    }
        
    // test clear()
    @Test
    public void testClear() {
        // check clearing on something that is already empty
        stringMPQ.clear();
        assertEquals("clear should set size to zero", 0, stringMPQ.size());
                
        // test clear sets size to zero     
        intMPQ.insert(4);
        stringMPQ.insert("one");
        intMPQ.insert(2);
        stringMPQ.insert("two");
        intMPQ.insert(3);
        stringMPQ.insert("three");
        intMPQ.insert(9);
        stringMPQ.insert("four");
        intMPQ.insert(5);
        stringMPQ.insert("five");
        intMPQ.insert(3);
        stringMPQ.insert("six");
        // now clear it 
        intMPQ.clear();
        stringMPQ.clear();
        assertEquals("clear should set size to zero", 0, intMPQ.size());
        assertEquals("clear should set size to zero", 0, stringMPQ.size());
        assertTrue("clear should make it empty", intMPQ.isEmpty());
        assertTrue("clear should make it empty", stringMPQ.isEmpty());

    }
    
    //test getMax()
    @Test
    public void testGetMax() {
        
        // double check what the deal is with checking exceptions 
        
        // insert some things, make sure correct one is returned 
        intMPQ.insert(5);
        intMPQ.insert(3);
        intMPQ.insert(29);
        intMPQ.insert(13);
        intMPQ.insert(101);
        intMPQ.insert(6);
        assertEquals("maximum value should be returned", (Integer) 101, intMPQ.getMax());
        
        // test proper return when only one value has been put in (can do this above). 
        intMPQ.clear();
        intMPQ.insert(0);
        assertEquals("maximum value should be returned, even if only one entry", (Integer) 0, intMPQ.getMax());
        
        // test that it works for negatives  
        intMPQ.clear();
        intMPQ.insert(-5);
        intMPQ.insert(-3);
        intMPQ.insert(-29);
        intMPQ.insert(-13);
        intMPQ.insert(-101);
        intMPQ.insert(-6);
        intMPQ.insert(-1);
        assertEquals("maximum value should be returned, even if negative", (Integer) (-1), intMPQ.getMax());
        
        
        // get max should NOT be using an absolute value comparator 
        intMPQ.clear();
        intMPQ.insert(-100);
        intMPQ.insert(99);
        assertEquals(" getMax should NOT be using an absolute value comparator ", (Integer) 99, intMPQ.getMax());                
    }
    
    @Test
    public void testGetMaxDoesNotChangeSize() {
        stringMPQ.insert("hear is a word ");
        stringMPQ.insert("hear is another word ");
        stringMPQ.insert("hear is and another word ");
        stringMPQ.insert("hear is one more  word ");
        stringMPQ.insert("still more ");
        stringMPQ.insert("and more ");
        int s = stringMPQ.size();
                
        intMPQ.insert(5);
        intMPQ.insert(3);
        intMPQ.insert(29);
        intMPQ.insert(29);
        intMPQ.insert(29);
        int i = intMPQ.size();
        
        intMPQ.getMax();
        stringMPQ.getMax();
        assertEquals("getMax() should not change size", s, stringMPQ.size());
        assertEquals("getMax() should not change size", i, intMPQ.size());
        
        intMPQ.getMax();
        stringMPQ.getMax();
        assertEquals("getMax() should not change size", s, stringMPQ.size());
        assertEquals("getMax() should not change size", i, intMPQ.size());
        
    }
    
    
    //test getMax throws the correct exceptions     
    @Test (expected=QueueEmptyException.class)
    public void testGetMaxThrowsRightExceptions() {
        intMPQ.clear(); // dont really need this line
        intMPQ.getMax();               
    }
        
    
 // test removeMax() -- test exceptions for this too
    @Test
    public void removeMax() {
        assertEquals("checking size", 0, intMPQ.size());
        intMPQ.insert(5);
        intMPQ.insert(6);
        intMPQ.insert(7);
        assertEquals("checking size", 3, intMPQ.size());
        assertEquals("Max is 7, third value added", (Integer) 7, intMPQ.removeMax());
        assertEquals("Size decreases", 2, intMPQ.size());
        intMPQ.insert(8);
        intMPQ.insert(9);
        intMPQ.insert(150);
        intMPQ.insert(5);
        intMPQ.insert(30);
        intMPQ.insert(50);
        assertEquals("checking size", 8, intMPQ.size());
        assertEquals("Max is 150, added in the middle", (Integer) 150, intMPQ.removeMax());
        assertEquals("Size decreases", 7, intMPQ.size());
        
        // test that it works if only one entry
        intMPQ.clear();
        intMPQ.insert(-100);
        assertEquals("removeMax should get only value if there is only one", (Integer) (-100), intMPQ.removeMax());
    }

    @Test (expected=QueueEmptyException.class)
    public void removeMaxOnEmpty() {
        intMPQ.clear();
        intMPQ.removeMax(); // should throw an exception
    }

    @Test (expected=QueueEmptyException.class)
    public void removeMaxOnLess() {
        intMPQ.insert(1);
        intMPQ.removeMax();
        intMPQ.removeMax(); // this one should throw an exception
    }

    
    // test insert()
    @Test
    public void insertDuplicatesIncreasesSize() {
        intMPQ.insert(4);
        intMPQ.insert(4);
        assertFalse("should not be empty", intMPQ.isEmpty());
        assertEquals("size should be two", 2, intMPQ.size());
        intMPQ.insert(3);
        assertEquals("size should be three", 3, intMPQ.size());
        intMPQ.insert(3);
        assertEquals("size should be four", 4, intMPQ.size());
        
        stringMPQ.insert("four");
        stringMPQ.insert("four");
        assertFalse("should not be empty", stringMPQ.isEmpty());
        assertEquals("size should be two", 2, stringMPQ.size());
        stringMPQ.insert("three");
        assertEquals("size should be three", 3, stringMPQ.size());
        stringMPQ.insert("three");
        assertEquals("size should be four", 4, stringMPQ.size());
    }        
    
    @Test
    public void sizeUpdatesAfterInsert() { 
        // this is also a redundant test, but we liked having it near the
        // size tests and the insert tests
        intMPQ.insert(3);
        assertEquals("size should be one", 1, intMPQ.size());
        intMPQ.insert(10);
        assertEquals("size should be two", 2, intMPQ.size());
        intMPQ.insert(11);
        intMPQ.insert(12);
        intMPQ.insert(13);
        intMPQ.insert(14);
        assertEquals("size should be six", 6, intMPQ.size());
    }
    
    // test init 
    //import collection linkedList
    @Test
    public void initOnEmpty() {
        LinkedList<Integer> ints = new LinkedList<>();
        final int a = 10;
        for (int b = 0; b < a; b++) {
            ints.add(b);
        }
        assertTrue("Empty priority queue", intMPQ.isEmpty());
        intMPQ.init(ints);
        assertFalse("Is no longer empty", intMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 10", 10, intMPQ.size());
        assertEquals("Max number should be 9", (Integer) 9, intMPQ.removeMax());
        assertEquals("New max number should be 8", (Integer) 8, intMPQ.removeMax());
    }
    
    @Test
    public void initWithEmptyOnEmpty() {
        LinkedList<Integer> ints = new LinkedList<>();
        assertTrue("Empty priority queue", intMPQ.isEmpty());
        intMPQ.init(ints);
        assertTrue("Should be empty", intMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 0", 0, intMPQ.size());
    }

    
    @Test
    public void initOnSet() {
        LinkedList<Integer> ints = new LinkedList<>();
        final int a = 10;
        for (int b = 0; b < a; b++) {
            ints.add(b);
        }
        for (int c = 0; c < a; c++) {
            intMPQ.insert(2*c);
            intMPQ.insert(3*c); // insert twice 
        }
        assertFalse("Priority queue should have values", intMPQ.isEmpty());
        assertEquals("Size should be twenty", 20, intMPQ.size());
        intMPQ.init(ints);
        assertFalse("PQ still not empty", intMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 10", 10, intMPQ.size());
        assertEquals("Max number is 9", (Integer) 9, intMPQ.removeMax());
        assertEquals("New max number is 8", (Integer) 8, intMPQ.removeMax());
    }

    @Test
    public void initWithEmptyOnSet() {
        LinkedList<Integer> ints = new LinkedList<>();
        final int a = 10;
        for (int c = 0; c < a; c++) {
            intMPQ.insert(c);
        }
        assertFalse("Priority queue has values", intMPQ.isEmpty());
        intMPQ.init(ints); // give it an empty linked list 
        assertTrue("PQ is now empty", intMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 0", 0, intMPQ.size());
    }
    
    //Test init string
    //import collection linkedList
    @Test
    public void initOnEmptyString() {
        LinkedList<String> strings = new LinkedList<>();
        final int a = 10;
        for (int b = 0; b < a; b++) {
            String bo = b + "";
            strings.add(bo);
        }
        assertTrue("Empty priority queue", stringMPQ.isEmpty());
        stringMPQ.init(strings);
        assertFalse("Is no longer empty", stringMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 10", 10, stringMPQ.size());
        assertEquals("Max number is 9", "9", stringMPQ.removeMax());
        assertEquals("New max number is 8", "8", stringMPQ.removeMax());
    }
    
    @Test
    public void initWithEmptyOnEmptyString() {
        LinkedList<String> strings = new LinkedList<>();
        assertTrue("Empty priority queue", stringMPQ.isEmpty());
        stringMPQ.init(strings);
        assertTrue("It should be still empty", stringMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 0", 0, stringMPQ.size());
    }

    
    @Test
    public void initOnSetString() {
        LinkedList<String> strings = new LinkedList<>();
        final int a = 10;
        final int d = 20;
        for (int b = 0; b < a; b++) {
            String bo = b + "";
            strings.add(bo);
        }
        for (int c = 4; c < d; c++) {
            String co = c + "";
            stringMPQ.insert(co);
        }
        assertFalse("Priority queue has values", stringMPQ.isEmpty());
        stringMPQ.init(strings); // should overwrite 
        assertFalse("PQ still not empty", stringMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 10", 10, stringMPQ.size());
        assertEquals("Max number is 9", "9", stringMPQ.removeMax());
        assertEquals("New max number is 8", "8", stringMPQ.removeMax());
    }

    @Test
    public void initWithEmptyOnSetString() {
        LinkedList<String> strings = new LinkedList<>();
        final int a = 10;
        for (int c = 0; c < a; c++) {
            String co = c + "";
            stringMPQ.insert(co);
        }
        assertFalse("Priority queue should have values", stringMPQ.isEmpty());
        stringMPQ.init(strings);
        assertTrue("PQ is now empty", stringMPQ.isEmpty());
        assertEquals("Size of newly initialized Priority Queue is 0", 0, stringMPQ.size());
    }



}
