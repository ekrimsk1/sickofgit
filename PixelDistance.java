/**Gabriela Mizrahi-Arnaud, Ben Gellman and Erez Krimsky.
 * gmizrah1 bgellma1 ekrimsk1
 * Data Structures Section 2 
 * Project 4
 *
 * This class implements the distance interface for pixels.
 * Distance is given by the sum of the squared differences of each component.
 */

public class PixelDistance implements Distance<Pixel> {
    
    /**Method returns distance between pixels.
     * Calculated as sum of square differences between R, G and B components.
     * @param one first pixel
     * @param two second pixel
     * @return double
     */
    public double distance(Pixel one, Pixel two) {
        double redDiff = one.getRed() - two.getRed();
        redDiff = Math.pow(redDiff, 2);
 
        double blueDiff = one.getBlue() - two.getBlue();
        blueDiff = Math.pow(blueDiff, 2);
 
        double greenDiff = one.getGreen() - two.getGreen();
        greenDiff = Math.pow(greenDiff, 2);
        return redDiff + blueDiff + greenDiff;
    }
}
