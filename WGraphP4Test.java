import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class WGraphP4Test {

    WGraphP4<String> g;
    GVertex<String> v, u, x, y;
    WEdge<String> e, f;

    @Before
    public void setupGraph() {
       g = new WGraphP4(100);
        v = new GVertex("v", g.nextID());
        u = new GVertex("u", g.nextID());
        x = new GVertex("x", g.nextID());
        y = new GVertex("y", g.nextID());
        e = new WEdge(v, u, 5);
        f = new WEdge(v, x, 8);
    }

    @Test
    public void testEmpty() {
        assertEquals(0, g.numEdges());
        assertEquals(0, g.numVerts());
    }

    @Test
    public void testAddVertex() {
        assertEquals(0, g.numVerts());
        assertEquals(true, g.addVertex(v)); // giving an issue
        assertEquals(1, g.numVerts());
        assertEquals(true, g.addVertex(u));
        assertEquals(2, g.numVerts());
        assertEquals(false, g.addVertex(v)); // add false if already there 
        assertEquals(true, g.allVertices().contains(v));
        assertEquals(true, g.allVertices().contains(u));
        assertEquals(false, g.allVertices().contains(x));
        // should test get that it is there!
    }
    

    @Test
    public void testAddEdge() {
        //System.out.println("test add edge");
        assertEquals(0, g.numEdges());
        assertEquals(true, g.addEdge(e));
        assertEquals(1, g.numEdges());
        assertEquals(true, g.addEdge(v, x, 8));
        assertEquals(2, g.numEdges());
        assertEquals(false, g.addEdge(v, u, 5)); //because e is v-u
        assertEquals(false, g.addEdge(f));
        assertEquals(true, g.allEdges().contains(e));

    }

    @Test
    public void testAdjacency() {
        g.addVertex(v);
        g.addVertex(u);
        g.addVertex(x);
        g.addVertex(y);
        assertEquals(false, g.areAdjacent(u, v));       
        g.addEdge(e);
        g.addEdge(f);
        assertEquals(true, g.areAdjacent(u, v));
        assertEquals(true, g.areAdjacent(v, u));
        assertEquals(true, g.areAdjacent(v, x));
        assertEquals(false, g.areAdjacent(x, u));
        assertEquals(false, g.areAdjacent(v, y));
    }

    @Test
    public void testIncidence() {
        g.addVertex(v);
        g.addVertex(u);
        g.addVertex(x);
        g.addVertex(y);
        g.addEdge(e);
        assertEquals(false, g.areIncident(e, x));
        assertEquals(false, g.areIncident(e, y));
        assertEquals(true, g.areIncident(e, v));
        assertEquals(true, g.areIncident(e, u));
        g.addEdge(f);
        assertEquals(true, g.areIncident(f, x));
        assertEquals(false, g.areIncident(f, u));
        assertEquals(4, g.numVerts());
        assertEquals(2, g.numEdges());
    }

    @Test
    public void testDegree() {
        g.addVertex(v);
        g.addVertex(u);
        g.addVertex(x);
        g.addVertex(y);
        assertEquals(0, g.degree(v));
        g.addEdge(e);
        assertEquals(1, g.degree(v));
        g.addEdge(f);
        assertEquals(2, g.degree(v));
        assertEquals(1, g.degree(x));
        assertEquals(0, g.degree(y));
    }


    @Test
    public void testNeighbors() {
        g.addVertex(v);
        g.addVertex(u);
        g.addVertex(x);
        g.addVertex(y);
        assertEquals("[]", g.neighbors(v).toString());
        g.addEdge(e);
        //        System.out.println(g.neighbors(v).toString());
        //assertEquals("[1]", g.neighbors(v).toString());
        assertEquals("[0]", g.neighbors(u).toString());
        g.addEdge(f);
        assertEquals("[1, 2]", g.neighbors(v).toString());
        assertEquals("[0]", g.neighbors(u).toString());
        assertEquals("[0]", g.neighbors(x).toString());
        assertEquals("[]", g.neighbors(y).toString());
    }

    @Test
    public void testRemoveEdges() {
        g.addVertex(v);
        g.addVertex(u);
        g.addVertex(x);
        g.addVertex(y);
        assertEquals(0, g.allEdges().size());
        assertEquals(0, g.numEdges());
        g.addEdge(e);
        assertEquals(true, g.allEdges().contains(e));
        assertEquals(false, g.allEdges().contains(f));
        g.addEdge(f);
        assertEquals(true, g.allEdges().contains(e));
        assertEquals(true, g.allEdges().contains(f));
        assertEquals(true, g.areAdjacent(u, v));
        assertEquals(true, g.areAdjacent(v, u));
        assertEquals(true, g.areAdjacent(v, x));
        assertEquals(false, g.areAdjacent(x, u));
        assertEquals(false, g.areAdjacent(v, y));
        g.deleteEdge(f.source(), f.end());
        assertEquals(true, g.areAdjacent(u, v));
        assertEquals(true, g.areAdjacent(v, u));
        assertEquals(false, g.areAdjacent(v, x));
        assertEquals(false, g.areAdjacent(x, u));
        assertEquals(false, g.areAdjacent(v, y));
        assertEquals(true, g.allEdges().contains(e));
        assertEquals(false, g.allEdges().contains(f));
        assertEquals(1, g.numEdges());
        assertEquals(2, g.allEdges().size());
        g.deleteEdge(e.source(), e.end()); 
        assertEquals(false, g.areAdjacent(u, v));
        assertEquals(false, g.areAdjacent(v, u));
        assertEquals(false, g.areAdjacent(v, x));
        assertEquals(false, g.areAdjacent(x, u));
        assertEquals(false, g.areAdjacent(v, y));
        assertEquals(0, g.numEdges());
        assertEquals(0, g.allEdges().size());
    }
    
    @Test
    public void testIncidentEdges() {
        g.addVertex(v);
        g.addVertex(u);
        g.addVertex(x);
        g.addVertex(y);
        g.addEdge(e);
        g.addEdge(f);
        WEdge h = new WEdge(x, y, 7);
        WEdge l = new WEdge(v, y, 8);
        g.addEdge(h);
        g.addEdge(l);
        assertEquals(true, g.incidentEdges(v).contains(e));
        assertEquals(true, g.incidentEdges(v).contains(f));
        assertEquals(false, g.incidentEdges(v).contains(h));
        assertEquals(true, g.incidentEdges(v).contains(l));

        assertEquals(true, g.incidentEdges(u).contains(e));
        assertEquals(false, g.incidentEdges(u).contains(f));
        assertEquals(false, g.incidentEdges(u).contains(h));
        assertEquals(false, g.incidentEdges(u).contains(l));

        assertEquals(false, g.incidentEdges(x).contains(e));
        assertEquals(true, g.incidentEdges(x).contains(f));
        assertEquals(true, g.incidentEdges(x).contains(h));
        assertEquals(false, g.incidentEdges(x).contains(l));

        assertEquals(false, g.incidentEdges(y).contains(e));
        assertEquals(false, g.incidentEdges(y).contains(f));
        assertEquals(true, g.incidentEdges(y).contains(h));
        assertEquals(true, g.incidentEdges(y).contains(l));
    }

    @Test
    public void testAllVertices() {
        System.out.println("Print all the vertices");
        System.out.println(g.allVertices());
    }
    @Test
    public void testKruskalsOnEmpty() {
        WGraphP4<Integer> emptyG = new WGraphP4(1);
        List<WEdge<Integer>> emptyL = new ArrayList<>();
        assertEquals(emptyL,emptyG.kruskals());
    }
    @Test
    public void testKruskals() {
        
        // need to test for the blank list -- getting issue 
        WGraphP4 graphForKrus1 = new WGraphP4(100);
        GVertex A = new GVertex('a', graphForKrus1.nextID());
        GVertex B = new GVertex('b', graphForKrus1.nextID());
        GVertex C = new GVertex('c', graphForKrus1.nextID());
        GVertex D = new GVertex('d', graphForKrus1.nextID());
        GVertex E = new GVertex('e', graphForKrus1.nextID());
        GVertex  F = new GVertex('f', graphForKrus1.nextID());
        WEdge e1 = new WEdge(A, B, 2);
        WEdge e2 = new WEdge(B, C, 5);
        WEdge e3 = new WEdge(A, C, 8);
        WEdge e4 = new WEdge(A, E, 6);
        WEdge e5 = new WEdge(E, F, 3);
        WEdge e6 = new WEdge(C, E, 8);
        WEdge e7 = new WEdge(C, D, 9);
        WEdge e8 = new WEdge(D, F, 4);
        WEdge e9 = new WEdge(B, D, 7);
        
        graphForKrus1.addVertex(A);
        graphForKrus1.addVertex(B);
        graphForKrus1.addVertex(C);
        graphForKrus1.addVertex(D);
        graphForKrus1.addVertex(E);
        graphForKrus1.addVertex(F);
        
        graphForKrus1.addEdge(e1);
        graphForKrus1.addEdge(e2);
        graphForKrus1.addEdge(e3);
        graphForKrus1.addEdge(e4);
        graphForKrus1.addEdge(e5);
        graphForKrus1.addEdge(e6);
        graphForKrus1.addEdge(e7);
        graphForKrus1.addEdge(e8);
        graphForKrus1.addEdge(e9);

        
        
        List<GVertex<String>> vertList1 = graphForKrus1.allVertices();
        System.out.println(vertList1.size());
        
        
        List<WEdge<String>> krusList1 =  graphForKrus1.kruskals();
        
        //System.out.print("Output of Kruskals ");
        //System.out.println(krusList1);
        
        // https://en.wikipedia.org/wiki/Kruskal%27s_algorithm
        WGraphP4 graphForKrus2 = new WGraphP4(100);
        GVertex AA = new GVertex('a', graphForKrus2.nextID());
        GVertex BB = new GVertex('b', graphForKrus2.nextID());
        GVertex CC = new GVertex('c', graphForKrus2.nextID());
        GVertex DD = new GVertex('d', graphForKrus2.nextID());
        GVertex EE = new GVertex('e', graphForKrus2.nextID());       
        WEdge ee1 = new WEdge(AA, BB, 3);
        WEdge ee2 = new WEdge(AA, EE, 1);
        WEdge ee3 = new WEdge(BB, EE, 4);
        WEdge ee4 = new WEdge(BB, CC, 5);
        WEdge ee5 = new WEdge(CC, DD, 2);
        WEdge ee6 = new WEdge(CC, EE, 6);
        WEdge ee7 = new WEdge(EE, DD, 7);
        
        
        graphForKrus2.addVertex(AA);
        graphForKrus2.addVertex(BB);
        graphForKrus2.addVertex(CC);
        graphForKrus2.addVertex(DD);
        graphForKrus2.addVertex(EE);
    
        
        graphForKrus2.addEdge(ee1);
        graphForKrus2.addEdge(ee2);
        graphForKrus2.addEdge(ee3);
        graphForKrus2.addEdge(ee4);
        graphForKrus2.addEdge(ee5);
        graphForKrus2.addEdge(ee6);
        graphForKrus2.addEdge(ee7);

        List<GVertex<String>> vertList2 = graphForKrus2.allVertices();
        System.out.println(vertList2.size());
        
        
        List<WEdge<String>> krusList2 =  graphForKrus2.kruskals();
        
        //System.out.print("Output of Kruskals ");
        //System.out.println(krusList2);
        
    }

    @Test
    public void testDepthFirst() {
        GVertex one = new GVertex("1", g.nextID());
        GVertex two = new GVertex("2", g.nextID());
        GVertex three = new GVertex("3", g.nextID());
        GVertex four = new GVertex("4", g.nextID());
        GVertex five = new GVertex("5", g.nextID());
        GVertex six = new GVertex("6", g.nextID());
        GVertex seven = new GVertex("7", g.nextID());
        GVertex eight = new GVertex("8", g.nextID());
        GVertex nine = new GVertex("9", g.nextID());
        GVertex ten = new GVertex("10", g.nextID());

        WEdge e1 = new WEdge(one, two, 1);
        WEdge e2 = new WEdge(one, five, 1);
        WEdge e3 = new WEdge(two, six, 1);
        WEdge e4 = new WEdge(two, ten, 1);
        WEdge e5 = new WEdge(three, eight, 1);
        WEdge e6 = new WEdge(four, three, 1);
        WEdge e7 = new WEdge(four, five, 1);
        WEdge e8 = new WEdge(four, nine, 1);
        WEdge e9 = new WEdge(five, three, 1);
        WEdge e10 = new WEdge(five, six, 1);
        //WEdge e11 = new WEdge(six, five, 1);
        WEdge e12 = new WEdge(six, seven, 1);
        WEdge e13 = new WEdge(six, ten, 1);
        WEdge e14 = new WEdge(seven, two, 1);
        WEdge e15 = new WEdge(seven, eight, 1);
        WEdge e16 = new WEdge(eight, nine, 1);
        WEdge e17 = new WEdge(nine, one, 1);
        WEdge e18 = new WEdge(nine, seven, 1);
        WEdge e19 = new WEdge(ten, one, 1);
        WEdge e20 = new WEdge(ten, four, 1);
        WEdge e21 = new WEdge(ten, nine, 1);

        WEdge[] edges = {e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21};

        for (int i = 0; i < edges.length; i++) {
            g.addEdge(edges[i]);
        }
        GVertex[] correct = {one, two, six, five, three, eight, nine, seven, ten, four};

        List<GVertex> depthfirsts = g.depthFirst(one);

        System.out.println("=========================================");

        for (int j = 0; j < correct.length; j++) {
            //System.out.println("j: " + j);
            System.out.println(correct[j].data() + ", " + depthfirsts.get(j).data()); 
            //assertEquals(correct[j].id(), depthfirsts.get(j).id());
        }

    }




}
