import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.Comparator;
import java.lang.Byte;

//these tests havent't been updated since they updated the info about
//RGB images
public class PixelTest {

    static Pixel a;
    static Pixel b;



    @Test
    public void testGetRed() {
        int hex = 0x8070F09;
        a = new Pixel(0, 1, hex);
        assertEquals(8, a.getRed());
    }
    @Test public void testGetBlue() {
        int hex = 0x8070F09;
        a = new Pixel(0, 1, hex);
        assertEquals(15, a.getBlue());
    }
    @Test public void testGetGreen() {
        int hex = 0x8070F09;
        a = new Pixel(0, 1, hex);
        assertEquals(7, a.getGreen());
   }


}
