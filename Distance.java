/**Gabriela Mizrahi-Arnaud, Ben Gellman and Erez Krimsky.
 * gmizrah1 bgellma1 ekrimsk1
 * Data Structures Section 2 
 * Project 4
 *
 * This is an interface for the distance between two values.
 * @param <T> generic
 */

interface Distance<T> {
    /**Method to return distance between two instances of T.
     * @param one first instance
     * @param two second instance
     * @return double
     */
    double distance(T one, T two);
}
